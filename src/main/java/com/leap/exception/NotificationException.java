package com.leap.exception;

/**
 * Copyright (C) MiA - Chalico Ltd.
 * Confidential and proprietary.
 */

/**
 *
 * Custom Exception to be thrown if there is any issue sending the notification
 *
 */
public class NotificationException extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public NotificationException(String message) {
        super(message);
    }

    /**
     *
     */
    public NotificationException(String message, Throwable cause) {
        super(message, cause);
    }
}
