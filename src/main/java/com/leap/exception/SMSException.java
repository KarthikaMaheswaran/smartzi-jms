package com.leap.exception;


/**
 *
 * Custom Exception to be thrown if there is any issue sending the SMS
 *
 */
public class SMSException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public SMSException(String message) {
        super(message);
    }

    public SMSException(String message, Throwable cause) {
        super(message, cause);
    }

}
