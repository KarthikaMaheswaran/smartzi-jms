package com.leap.exception.constant;

/**
 * Copyright (C) MiA - Chalico Ltd.
 * Confidential and proprietary.
 */


import com.leap.common.constant.LogMessage;

/**
 *
 * All Exception constants to go here
 *
 */
public class ExceptionConstant {

    private ExceptionConstant() {
        // private constructor
    }

    /**
     *
     */
    public static final int OTP_VALIDATION_FAILED = 1001;
    /**
     *
     */
    public static final int MISSING_REQUIRED_FIELD = 1002;
    /**
     *
     */
    public static final int RECORD_INSERT_FAILED = 1003;
    /**
     *
     */
    public static final int RECORD_FETCH_FAILED = 1004;
    /**
     *
     */
    public static final int UNIQUE_CONSTRAINT_FAILED = 1005;
    /**
     *
     */
    public static final int JSON_PARSING_FAILED = 1006;
    /**
     *
     */
    public static final int UPLOAD_FILE_FAILED = 1007;
    /**
     *
     */
    public static final int INVALID_USERID_OR_PASSWORD = 1008;
    /**
     *
     */
    public static final int RECORD_DOES_NOT_EXIST = 1009;
    /**
     *
     */
    public static final int ERROR_IN_QUEUE = 1010;
    /**
     *
     */
    public static final int RULE_FETCH_FAILED = 1011;
    /**
     *
     */
    public static final int USER_TYPE_MISMATCH = 1012;
    /**
     *
     */
    public static final int DRIVER_ALREADY_REGISTERED = 1013;
    /**
     *
     */
    public static final int DRIVER_DEVIATION_ALREADY_EXIST = 1014;
    /**
     *
     */
    public static final int DRIVER_AVAILABILITY_ALREADY_EXIST = 1015;
    /**
     *
     */
    public static final int PRE_BOOKING_REQUEST = 1016;
    /**
     *
     */
    public static final int TRAVEL_DATE_TIME_ALREADY_PASSED = 1017;
    /**
     *
     */
    public static final int AREA_UNCOVERED = 1018;
    /**
     *
     */
    public static final int CREDIT_CARD_IS_NOT_REGISTERED = 1019;
    /**
     *
     */
    public static final int CREDIT_CARD_IS_NOT_VALID = 1020;
    /**
     *
     */
    public static final int FORGOT_PASSWORD_URL_EXPIRED = 1021;
    /**
     *
     */
    public static final int FORGOT_PASSWORD_URL_INVALID = 1022;
    /**
     *
     */
    public static final int POSTCODE_FILE_INVALID = 1023;
    /**
     *
     */
    public static final int PAYMENT_UNSUCCESSFUL = 1024;
    /**
     *
     */
    public static final int INVALID_PROFILE_TO_EDIT = 1025;
    /**
     *
     */
    public static final int INVALID_PASSWORD = 1026;
    /**
     *
     */
    public static final int SELECTED_DRIVER_BOOKED = 1027;
    /**
     *
     */
    public static final int REQUEST_TIMEOUT = 1028;
    /**
     *
     */
    public static final int TRIP_HAS_ALREADY_STARTED = 1029;
    /**
     *
     */
    public static final int USER_BLACKLISTED = 1030;
    /**
     *
     */
    public static final int USER_DEACTIVATED = 1031;
    /**
     *
     */
    public static final int ACCOUNT_DELETED = 1032;
    /**
     *
     */
    public static final int TRIP_IS_ALREADY_CANCELLED = 1033;
    /**
     *
     */
    public static final int USER_IS_NOT_CUSTOMER = 1034;
    /**
     *
     */
    public static final int INVALID_USER_STATE_TO_PERFORM_THIS_ACTION = 1035;
    /**
     *
     */
    public static final int FILE_NOT_FOUND = 1036;
    /**
     *
     */
    public static final int FILE_ALREADY_PROCESSED = 1037;

    /**
     *
     */
    public static final int PREFERRED_DRIVER_LIMIT = 1038;
    /**
     *
     */
    public static final int VERSION_MISMATCH = 1039;
    /**
     *
     */
    public static final int PREFERREDDRIVER_NOT_EXIST = 1040;

    /**
     *
     */
    public static final int DRIVER_NOT_AVAILABLE = 1041;
    /**
     *
     */
    public static final int Account_ERROR_TYPE_1 = 1042;
    /**
     *
     */
    public static final int Account_ERROR_TYPE_2 = 1043;
    /**
     *
     */
    public static final int Account_ERROR_TYPE_3 = 1044;

    /**
     *
     */
    public static final int DRIVER_CODE_EXIST = 1045;


    public static final int Card_ERROR_TYPE_EXP_MON = 1046;
    /**
     *
     */
    public static final int Card_ERROR_TYPE_EXP_YEAR = 1047;
    /**
     *
     */
    public static final int Card_ERROR_TYPE_INV_CVC = 1048;

    /**
     *
     */
    public static final int Card_ERROR_TYPE_INC_NUM = 1049;

    /**
     *
     */
    public static final int Card_ERROR_TYPE_Default = 1050;

    /**
     *
     */
    public static final int Card_ERROR_TYPE_IVALIDNUMBER = 1051;

    /**
     *
     */
    public static final int Card_ERROR_TYPE_CARD_DEC = 1052;

    /**
     *
     */
    public static final int Card_ERROR_TYPE_CARD_EXP = 1053;

    /**
     *
     */
    public static final int STRIPE_API_AUTH_ERR = 1054;


    public static final int DRIVER_EMAIL_NOT_EXIST = 1055;
    /**
     *
     */
    public static final int ALREADY_REGISTERD = 1056;
    /**
     *
     */
    public static final int DRIVER_DEACTIVATED = 1057;
    /**
     *
     */
    public static final int DRIVER_BLACKLISTED = 1058;
    /**
     *
     */
    public static final int DRIVER_NEWLY_REGISTERED = 1059;
    /**
     *
     */
    public static final int DRIVER_REJECTED = 1060;
    /**
     *
     */
    public static final int DRIVER_ACTIVE = 1061;
    /**
     *
     */
    public static final int CUSTOMER_STRIPE_ACC_CREATE_ERROR = 1062;
    /**
     *
     */
    public static final int DRIVER_DETAILS_WRONG = 1063;
    /**
     *
     */
    public static final int DRIVER_MAXIMUM_RATE = 1064;
    /**
     *
     */
    public static final int CARD_FAILURE_ERROR = 1065;
    /**
     *
     */
    public static final int INVLID_COUPON = 1066;
    /**
     *
     */
    public static final int COUPON_EXIST = 1067;

    /**
     *
     */
    public static final int DRIVER_DELETED = 1068;

    /**
     *
     */
    public static final int PROMO_PER_USER_VALIDATION = 1069;
    /**
     *
     */
    public static final int PROMO_BUDGET_VALIDATION = 1070;
    /**
     *
     */
    public static final int PROMO_INVALID = 1071;
    /**
     *
     *
     */
    public static final int COMMON_EXCEPTION = 1072;

    /**
     *
     *
     */
    public static final int LONG_DSTANCE_DISCOUNT_EXCEPTION = 1073;

    /**
     *
     *
     */
    public static final int VALID_DSICOUNT_PERCENTAGE = 1074;
    /**
     *
     *
     *
     */
    public static final int ACCOUNT_UNIQUE_CONSTRAINT_FAILED = 1075;
    /**
     *
     */
    public static final int TARIFF_ALREADY_EXIST = 1076;
    /**
     *
     */
    public static final int RECORD_DELETED_SUCCESSFULLY = 1077;

    /**
     *
     */
    public static final int TRIP_ALREADY_BOOKED = 1078;

    /**
     *
     */
    public static final int INVALID_REQUEST = 1079;

    /**
     *
     */
    public static final int INVALID_DISTANCE = 1080;


    /**
     *
     */
    public static final int UNABLE_UPDATE = 1081;

    /**
     *
     */
    public static final int PROMO_EXPAIRY = 1082;


    /**
     *
     */
    public static final int NO_INVOICE_GENERATE = 1083;


    /**
     *
     * @param code
     * @return
     */

    public static final int CARD_BOOKING_NOT_ALLOWED = 1084;

    /**
     *
     */
    public static final int INVALID_OLD_PASSWORD = 1085;

    /**
     *
     * @param code
     * @return
     */
    public static final int INVALID_EMAIL = 1086;
    /**
     *
     */

    public static final int INSUFFICIENT_BALANCE = 1087;
    /**
     *
     */
    public static final int INSUFFICIENT_BALANCE_FOR_DRIVER = 1088;

    /**
     *
     */

    public static final int CANNOT_PULL_JOB = 1089;

    /**
     *
     */

    public static final int CANNOT_ACCEPT__PAST_JOB = 1090;

    /**
     *
     * @param code
     * @return
     */
    public static final int INVALID_VEHICLE_TYPE_ID = 1091;

    /**
     *
     * @param code
     * @return
     */
    public static final int INVALID_CARD_DETAIL = 1092;
    /**
     *
     * @param code
     * @return
     */
    public static final int ACCOUNT_ERROR_TYPE_4 = 1093;


    public static String getDescription(int code) {
        String message = "";
        switch (code) {
            case 1001:
                message = "Incorrect OTP. Please try again.";
                break;
            case 1002:
                message = "Mandatory fields cannot be left blank.";
                break;
            case 1003:
                message = LogMessage.TROUBLE_WITH_SERVER;
                break;
            case 1004:
                message = LogMessage.TROUBLE_WITH_SERVER;
                break;
            case 1005:
                message = "This email ID is already registered with Smartzi.";
                break;
            case 1006:
                message = "Sorry, something went wrong. Please try again.";
                break;
            case 1007:
                message = LogMessage.TROUBLE_WITH_SERVER;
                break;
            case 1008:
                message = "Invalid Username or Password. Please try again.";
                break;
            case 1009:
                message = "User is not registered with Smartzi.";
                break;
            case 1010:
                message = LogMessage.TROUBLE_WITH_SERVER;
                break;
            case 1012:
                message = "Invalid Login. Please try again.";
                break;
            case 1013:
                message = "This driver is already registered with Smartzi.";
                break;
            case 1014:
                message = "An exception alreay exists for the selected duration.";
                break;
            case 1015:
                message = "Driver availability already exits.";
                break;
            case 1016:
                message = "Successful pre-booking request.";
                break;
            case 1017:
                message = "Please check pickup time - it cannot be in the past.";
                break;
            case 1018:
                message = "Smartzi is coming to your area soon. We will get in touch to tell you when.";
                break;
            case 1019:
                message = "Could not be activated! Please enter a valid card detail for the driver.";
                break;
            case 1020:
                message = "Invalid credit card. Please update your card details.";
                break;
            case 1021:
                message = "Forgot password URL has expired.";
                break;
            case 1022:
                message = "Invalid URL request.";
                break;
            case 1023:
                message = "Invalid post code file uploaded.";
                break;
            case 1024:
                message = "Payment unsuccessful.";
                break;
            case 1025:
                message = "You cannot edit selected profile.";
                break;
            case 1026:
                message = "Incorrect password entered.";
                break;
            case 1027:
                message = "Driver is unable to accept your ride request. Please try again later.";
                break;
            case 1028:
                message = "Your request has timed-out. Please try again.";
                break;
            case 1029:
                message = "Selected trip has already started.";
                break;
            case 1030:
                message = LogMessage.ACCOUNT_DEACTIVATED;
                break;
            case 1031:
                message = LogMessage.ACCOUNT_DEACTIVATED;
                break;
            case 1032:
                message = LogMessage.ACCOUNT_DEACTIVATED;
                break;
            case 1033:
                message = "This trip is already cancelled.";
                break;
            case 1034:
                message = "This email ID is not a customer.";
                break;
            case 1035:
                message = "You are not allowed to perform this action.";
                break;
            case 1036:
                message = "File not found";
                break;
            case 1037:
                message = "File already processed";
                break;
            case 1038:
                message = "Sorry, you have already selected the maximum number of preferred drivers.";
                break;
            case 1039:
                message = "New version of the app is available to download. Please update and take advantage of the new features.";
                break;
            case 1040:
                message = "Invalid driver code. Please try again with the correct code.";
                break;
            case 1041:
                message = "No driver is currently available for your travel time. Please try again later.";
                break;
            case 1042:
                message = "Invalid post code. Please try again with a valid code.";
                break;
            case 1043:
                message = "Invalid sort code. Please try again with a valid code.";
                break;
            case 1044:
                message = "Invalid account number. Please try again.";
                break;
            case 1045:
                message = "Driver already exists.";
                break;
            case 1046:
                message = "Invalid Expiry Details. Please try again.";
                break;
            case 1047:
                message = "Invalid Expiry Details. Please try again.";
                break;
            case 1048:
                message = "Invalid CVC Details. Please try again.";
                break;
            case 1049:
                message = "The card details entered are invalid. Please check and try again.";
                break;
            case 1050:
                message = "The card details entered are invalid. Please check and try again.";
                break;
            case 1051:
                message = "The card details entered are invalid. Please check and try again.";
                break;
            case 1052:
                message = "The card details entered are invalid. Please check and try again.";
                break;
            case 1053:
                message = "Your card has expired. Please try again with valid card details.";
                break;
            case 1054:
                message = "Connection failure. Please try again.";
                break;
            case 1055:
                message = "Email address does not exist.";
                break;
            case 1056:
                message = "Email address is already registered.";
                break;
            case 1057:
                message = "Email address is deactivated. Please contact the Admin Team.";
                break;
            case 1058:
                message = "Email address is blacklisted. Please contact the Admin Team.";
                break;
            case 1059:
                message = "Email address is already registered pending Admin Team approval.";
                break;
            case 1060:
                message = "Email address is rejected. Please contact the Admin Team.";
                break;
            case 1061:
                //message = "Given email address is rejected.Please contact admin team.";
                break;
            case 1062:
                message = "Customer credit card failed. Please check of update to continue.";
                break;
            case 1063:
                message = "Please enter valid driver email.";
                break;
            case 1064:
                message = "Rate too high. Please enter a below market value.";
                break;
            case 1065:
                message = "Card payment unsuccessfull. Please continue with cash payment to complete trip.";
                break;
            case 1066:
                message = "Invalid promo code. Please try again with a valid code.";
                break;
            case 1067:
                message = "Invalid promo code. Please try again with a valid code.";
                break;
            case 1068:
                message = "Your account no longer exists. Please contact Smartzi customer suppport.";
                break;
            case 1069:
                message = "You have exceeded the promo usage limit.";
                break;
            case 1070:
                message = "Promo Expired. Please try again with a valid promo code.";
                break;
            case 1071:
                message = "Invalid promo code. Please try again with a valid code.";
                break;
            case 1072:
                message = "Sorry, something went wrong. Please try again later.";
                break;
            case 1073:
                message = "The discount percentage cannot be less than the default value. Please set a higher discount percentage or set it at the default value.";
                break;
            case 1074:
                message = "Please enter the valid discount percentage.";
                break;
            case 1075:
                message = "This email ID and account name is already registered with Smartzi.";
                break;
            case 1076:
                message = "This tariff already exists";
                break;
            case 1077:
                message ="Selected Recorded Successfully deleted.";

            case 1078:
                message ="Sorry, this ride has expired and not available for you anymore.";
                break;


            case 1079:
                message ="Invalid request.";
                break;

            case 1080:
                message ="Please check the source and the destination.";
                break;

            case 1081:
                message ="Unable to update booking information";
                break;

            case 1082:
                message ="promo code expired";
                break;

            case 1083:
                message="No Invoice Generated";
                break;

            case 1084:
                message = "Card Booking Not Allowed for Guest";
                break;

            case 1085:
                message = "Current password is invalid. Please try again";
                break;

            case 1086:
                message = "Email Id Does not Exist";
                break;

            case 1087:
                message = "I am sorry, your card has been declined. If you would like to continue with your booking please choose cash payment to proceed";
                break;
            case 1088:
                message = "I am sorry, your card has been declined. If you would like to continue with your booking please choose cash / account payment to proceed";
                break;

            case 1089:
                message = "You cannot pull job which is not in driver job pool";
                break;

            case 1090:
                message = "You cannot accept past job from driver job pool";
                break;
            case 1091:
                message = "Invalid VehicleTypeId";
                break;
            case 1092:
                message = "Invalid card detail";
                break;
            case 1093:
                message = "Sorry, Card payment has been declined. If you would like to continue with your booking please choose account payment to proceed";
                break;
            case 1094:
                message = "Unable to add the Payment Account. Please contact the Admin Team.";
                break;
            default:
                message = "";
                break;
        }
        return message;
    }

}
