package com.leap.exception;

/**
 *
 * Custom Runtime Exception to be thrown if there is any issue performing DB
 * operation
 *
 */
public class DBException extends LeapException {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public DBException(Throwable cause, int code) {
        super(cause, code);
    }

}
