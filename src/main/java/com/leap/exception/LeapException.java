package com.leap.exception;
/**
 * Copyright (C) MiA - Chalico Ltd.
 * Confidential and proprietary.
 */


/**
 * Custom Runtime Exception which will be wrapped over any exception occurring
 * before sending it to client
 *
 */
public class LeapException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    protected int code;

    protected String message;

    /**
     *
     * @param cause cause
     * @param code code
     */
    public LeapException(Throwable cause, int code) {
        super(cause);
        this.code = code;
    }

    /**
     *
     * @param message message
     * @param cause cause
     * @param code code
     */
    public LeapException(String message, Throwable cause, int code) {
        super(message, cause);
        this.code = code;
    }

    /**
     *
     * @param message message
     */
    public LeapException(String message) {
        super(message);
    }

    /**
     *
     * @param code code
     */
    public LeapException(int code) {
        this.code = code;
    }

    /**
     *
     * @return int code
     */
    public int getCode() {
        return code;
    }

    /**
     *
     * @param code code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     *
     * @param message message
     * @param code code
     */
    public LeapException(String message, int code) {
        super(message);
        this.code = code;
    }
}

