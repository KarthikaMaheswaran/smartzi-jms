package com.leap.exception;


/**
 * Custom Exception to be thrown if there is any issue sending the Push
 * notification
 *
 */
public class PNSException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public PNSException(String message) {
        super(message);
    }

    /**
     *
     */
    public PNSException(String message, Throwable cause) {
        super(message, cause);
    }
}

