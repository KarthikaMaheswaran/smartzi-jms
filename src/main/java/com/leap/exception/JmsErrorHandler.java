package com.leap.exception;


import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.ErrorHandler;

@Service
public class JmsErrorHandler  implements ErrorHandler {

    private static final Logger LOG = Logger.getLogger(JmsErrorHandler.class);
    @Override
    public void handleError(Throwable t) {
        LOG.info("An error has occurred in the JMS : "+ t.getMessage());
    }
}
