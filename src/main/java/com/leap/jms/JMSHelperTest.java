package com.leap.jms;

import com.google.gson.Gson;
import org.apache.activemq.ScheduledMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.leap.jms.dto.NotificationMessage;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import java.io.Serializable;
import java.util.logging.Logger;

@Component
public class JMSHelperTest {

    private static final Logger LOG = Logger.getLogger(String.valueOf(JMSHelperTest.class));

    @Autowired
    private JMSProducerTest jmsProducer;

    /**
     * This method convert a message to serialized message.
     *
     * @param obj
     *            message object
     * @param session
     *            is the Queue session
     * @return serialized message object
     * @throws JMSException JMSException
     */
    Gson gson = new Gson();
    public ObjectMessage createObjectMessage(Object obj, Session session) throws JMSException {
        LOG.info("obj*************"+gson.toJson(obj));
        ObjectMessage message = session.createObjectMessage();
        LOG.info("message**********"+gson.toJson(message));


        message.setObject((Serializable) obj);
        message.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_DELAY, 1000);
        message.setStringProperty("apns-push-type", "alert");
//        message = session.createObjectMessage();
//        HashMap hm = new HashMap();
//        hm.put("apns-push-type", "alert");
//        message.setObject(hm);
        LOG.info("message-2-**********"+gson.toJson(message));

        return message;
    }

    /**
     *
     */
    public void pushToQueue(NotificationMessage notificationMessage, int priority) throws JMSException {
        switch (priority) {
            case 1:
                jmsProducer.generateMessages(notificationMessage,"low_priority.queue");
                break;
            case 2:
                jmsProducer.generateMessages(notificationMessage, "med_priority.queue");
                break;
            case 3:
                jmsProducer.generateMessages(notificationMessage, "high_priority.queue");
                break;
            default:
                break;
        }

    }

    public interface Priority {
        /**
         *
         */
        int HIGH = 1;
        /**
         *
         */
        int MEDIUM = 2;
        /**
         *
         */
        int LOW = 3;
    }
}
