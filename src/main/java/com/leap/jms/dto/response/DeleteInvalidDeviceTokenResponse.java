package com.leap.jms.dto.response;

/**
 * @author Hisoban
 * @date 04-Aug-20
 */
public class DeleteInvalidDeviceTokenResponse {

    private boolean res;
    private int statusCode;
    private String msg;

    public boolean isRes() {
        return res;
    }

    public void setRes(boolean res) {
        this.res = res;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}

