package com.leap.jms.dto.request;

public class NotificationRequest {
    private String notificationGUID;
    private int priority;

    public String getNotificationGUID() {
        return notificationGUID;
    }

    public void setNotificationGUID(String notificationGUID) {
        this.notificationGUID = notificationGUID;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
