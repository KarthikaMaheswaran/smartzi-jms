package com.leap.jms.dto.request;

/**
 * @author Hisoban
 * @date 04-Aug-20
 */
public class DeleteInvalidDeviceTokenRequest {

    private String deviceRegistrationToken;
    private short appPlatformId;

    public String getDeviceRegistrationToken() {
        return deviceRegistrationToken;
    }

    public void setDeviceRegistrationToken(String deviceRegistrationToken) {
        this.deviceRegistrationToken = deviceRegistrationToken;
    }

    public short getAppPlatformId() {
        return appPlatformId;
    }

    public void setAppPlatformId(short appPlatformId) {
        this.appPlatformId = appPlatformId;
    }
}

