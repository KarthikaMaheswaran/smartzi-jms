package com.leap.jms.dto;

import java.io.Serializable;
/**
 *
 * DTO to hold notification message
 *
 */
public class NotificationMessage implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    private String notificationReqGUID;

    /**
     *
     */
    public NotificationMessage(String notificationReqGUID) {
        this.notificationReqGUID = notificationReqGUID;
    }

    /**
     *
     */
    public String getNotificationReqGUID() {
        return notificationReqGUID;
    }

    /**
     *
     */
    public void setNotificationReqGUID(String notificationReqGUID) {
        this.notificationReqGUID = notificationReqGUID;
    }

}
