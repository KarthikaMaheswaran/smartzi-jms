package com.leap.jms.dto;


/**
 * DTO to hold device info
 *
 */
public class DeviceInfoDTO {

    /**
     *
     */
    private String deviceRegistrationToken;
    /**
     *
     */
    private Short platformType;
    /**
     *
     */
    private Short appType;

    /**
     * @return the deviceRegistrationToken
     */
    public String getDeviceRegistrationToken() {
        return deviceRegistrationToken;
    }

    /**
     * @param deviceRegistrationToken
     *            the deviceRegistrationToken to set
     */
    public void setDeviceRegistrationToken(String deviceRegistrationToken) {
        this.deviceRegistrationToken = deviceRegistrationToken;
    }

    /**
     * @return the platformType
     */
    public Short getPlatformType() {
        return platformType;
    }

    /**
     * @param platformType
     *            the platformType to set
     */
    public void setPlatformType(Short platformType) {
        this.platformType = platformType;
    }

    /**
     * @return the appType
     */
    public Short getAppType() {
        return appType;
    }

    /**
     * @param appType
     *            the appType to set
     */
    public void setAppType(Short appType) {
        this.appType = appType;
    }

}

