package com.leap.jms;

/**
 * Copyright (C) MiA - Chalico Ltd.
 * Confidential and proprietary.
 */

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import com.leap.dao.NotificationRequestDAO;
import com.leap.entity.NotificationReqEntity;
import com.leap.exception.NotificationException;
import com.leap.notification.service.NotificationService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import com.leap.jms.dto.NotificationMessage;
import com.leap.exception.DBException;
import com.leap.notification.model.NotificationRequest;


import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.io.IOException;

/**
 * Listener to check requests in the queue
 */
@Component
//@EnableJms
public class JMSListenerTest implements MessageListener {

    /**
     *
     */
    private final org.slf4j.Logger LOG = LoggerFactory.getLogger(this.getClass());


    static {
        System.setProperty("org.apache.activemq.SERIALIZABLE_PACKAGES", "*");
    }

    /**
     *
     */
    @Autowired
    private NotificationRequestDAO notificationRequestDAO;
    /**
     *
     */
    @Autowired
    private NotificationService notificationService;


    @Override
//    @JmsListener(destination = "low_priority.queue", id = "low_priority.queue")
//    @JmsListener(destination = "high_priority.queue", id = "high_priority.queue")
//    @JmsListener(destination = "dlqListenerContainer", id = "dlqListenerContainer")
//    @JmsListener(destination = "medium_priority.queue", id = "medium_priority.queue")
    public void onMessage(Message message) {
        if (LOG.isInfoEnabled()) {
            LOG.info("test-In the JMS LISTENER TEST ***");
        }
        Gson gson = new Gson();
        ObjectMessage objMsg = (ObjectMessage) message;
             try {
            if (objMsg.getObject() instanceof NotificationMessage) {

                NotificationMessage notificationMessage = (NotificationMessage) objMsg.getObject();

                if (LOG.isInfoEnabled()) {
                    LOG.info("test-Fetched Notification GUID ***" + notificationMessage.getNotificationReqGUID());
                }
                NotificationReqEntity notificationReqEntity = notificationRequestDAO
                        .getNotificaitonRequest(notificationMessage.getNotificationReqGUID());
                LOG.info("test-JMSListener - onMessage:notificationReqEntity: >>>>>>>>>>>>>>" + gson.toJson(notificationReqEntity));
//                if(notificationReqEntity != null) {
                String messageRequest = notificationReqEntity.getRequestPayLoad();
                ObjectMapper mapper = new ObjectMapper();
                NotificationRequest notificationRequest = mapper.readValue(messageRequest, NotificationRequest.class);
                notificationService.sendMessage(notificationRequest, notificationReqEntity.getNotificationRequestId());
                //                notificationRequestDAO.updateNotificationRequestStatus(
                //                        notificationReqEntity.getNotificationRequestGUID(), ApplicationConstant.RequestStatus.PROCESSED);
//                }
            }
        } catch (ClassCastException e) {
            LOG.error("ClassCastException occurred while trying to process queue, Cause : " + e.toString());
        } catch (JsonParseException e) {
            LOG.error("JsonParseException occurred while trying to process queue, Cause : " + e.toString());
        } catch (JsonMappingException e) {
            LOG.error("JsonMappingException occurred while trying to process queue, Cause : " + e.toString());
        } catch (IOException e) {
            LOG.error("IOException occurred while trying to process queue, Cause : " + e.toString());
        } catch (DBException e) {
            LOG.error("DBException occurred while trying to process queue, Cause : " + e.toString());
        } catch (JMSException e) {
            LOG.error("JMSException occurred while trying to process queue, Cause : " + e.toString());
        } catch (NotificationException e) {
            LOG.error("NotificationException occurred while trying to process queue, Cause : " + e.toString());
            /*
            if (StringUtils.equalsIgnoreCase(e.getMessage(), "Retry")) {
                // Throwing exception if sending of notification has failed and
                // needs to be retried
                throw new UnsupportedOperationException();
            } else {
                LOG.error("NotificationException occurred while trying to process queue, Cause : " + e.toString());
            }
            */
        }
    }


}
