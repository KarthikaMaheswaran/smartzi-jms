package com.leap.jms;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;
import com.leap.jms.dto.NotificationMessage;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;
@Component
public class JMSProducerTest {
    /**
     *
     */
    public static final Logger LOG = Logger.getLogger(JMSProducerTest.class);
    /**
     *
     */

    static {
        System.setProperty("org.apache.activemq.SERIALIZABLE_PACKAGES", "*");
    }
    @Autowired
    private JmsTemplate jmsTemplate;
    /**
     *
     */
    @Autowired
    private JMSHelperTest jmsHelper;


    /**
     * Generates JMS messages
     */
    public void generateMessages(final NotificationMessage notification, final String destination) throws JMSException {
        if (LOG.isInfoEnabled()) {
            LOG.info("Pushed Notification GUID ... " + notification.getNotificationReqGUID());
            LOG.info("Pushed Notification destination ... " + destination);
        }
        jmsTemplate.send(destination, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                ObjectMessage message = jmsHelper.createObjectMessage(notification, session);
                if (LOG.isInfoEnabled()) {
                    LOG.info("Pushing notification message to queue.");
                }
                return message;
            }
        });
    }
}
