package com.leap.dao;

import com.leap.entity.EmailNotificationEntityV2;
import com.leap.exception.DBException;

public interface EmailNotificationDAO {
    /**
     * This method is used to insert SMS Notification data into the DB
     *
     * @param emailNotificationEntity
     * @return long
     * @throws DBException DBException
     */
    Boolean insertEmailNotification(EmailNotificationEntityV2 emailNotificationEntity) throws DBException;

}
