package com.leap.dao;

import com.leap.entity.SMSNotificationEntity;
import com.leap.entity.SMSNotificationEntityV2;
import com.leap.exception.DBException;

/**
 * DataBase operations related to SMS Notification for the application
 */
public interface SMSNotificationDAO {

    /**
     * This method is used to insert SMS Notification data into the DB
     *
     * @param smsNotificationEntity smsNotificationEntity
     * @return long
     * @throws DBException DBException
     */
    Long insertOrUpdate(SMSNotificationEntity smsNotificationEntity) throws DBException;

    Boolean insertOrUpdate_V2(SMSNotificationEntityV2 smsNotificationEntityV2) throws DBException;

    /**
     * This method is used to update the status of message delivery
     *
     * @param messageId messageId
     * @param status status
     * @param deliverySummary deliverySummary
     * @return long
     */
    Long updateMsgStatus(String messageId, Byte status, String deliverySummary);
}

