package com.leap.dao;
import com.leap.entity.NotificationReqEntity;
/**
 * Copyright (C) MiA - Chalico Ltd.
 * Confidential and proprietary.
 */


import com.leap.exception.DBException;

/**
 * DataBase operations related to Notification for the application
 */
public interface NotificationRequestDAO {

    /**
     * This method is used to insert Notification request data into the DB
     *
     * @param notificationRequestEntity notificationRequestEntity
     * @return long
     * @throws DBException DBException
     */
    Long insertOrUpdate(NotificationReqEntity notificationRequestEntity) throws DBException;

    /**
     * This method is used to fetch Notification request data from the DB
     *
     * @param notificationRequestGUID notificationRequestGUID
     * @return notificationreqentity
     * @throws DBException DBException
     */
    NotificationReqEntity getNotificaitonRequest(String notificationRequestGUID) throws DBException;

    /**
     * @param notificationRequestGUID notificationRequestGUID
     * @param status                  status
     * @return long
     * @throws DBException DBException
     */
    Long updateNotificationRequestStatus(String notificationRequestGUID, int status) throws DBException;

}
