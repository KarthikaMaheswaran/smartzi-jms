package com.leap.dao.impl;


import com.leap.dao.SMSNotificationDAO;
import com.leap.dao.constant.CommonDAOConstant;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.leap.entity.SMSNotificationEntity;
import com.leap.entity.SMSNotificationEntityV2;
import com.leap.exception.DBException;
import com.leap.exception.constant.ExceptionConstant;

import java.sql.*;

@Repository
public class SMSNotificationDAOImpl implements SMSNotificationDAO {
    /**
     *
     */
    private final org.slf4j.Logger LOG = LoggerFactory.getLogger(this.getClass());
    /**
     *
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     *
     */
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional
    public Long insertOrUpdate(SMSNotificationEntity smsNotificationEntity) throws DBException {
        Long startTime = System.currentTimeMillis();
        Connection connection = null;
        CallableStatement callableSt = null;
        try {

            // Get Connection from dataSource
            connection = DataSourceUtils.getConnection(jdbcTemplate.getDataSource());
            callableSt = connection.prepareCall(CommonDAOConstant.StoredProcedure.INSERT_OR_UPDATE_SMS_NOTIFICATION);
            callableSt.setObject(1, smsNotificationEntity.getSmsNotificationId(), Types.BIGINT);
            callableSt.setObject(2, smsNotificationEntity.getNotificationRequestId(), Types.BIGINT);
            callableSt.setObject(3, smsNotificationEntity.getMobileNum(), Types.VARCHAR);
            callableSt.setObject(4, smsNotificationEntity.getMessage(), Types.VARCHAR);
            callableSt.setObject(5, smsNotificationEntity.getStatus(), Types.SMALLINT);
            callableSt.setObject(6, smsNotificationEntity.getMessageToken(), Types.VARCHAR);
            callableSt.setObject(7, smsNotificationEntity.getDeliverySummary(), Types.VARCHAR);
            callableSt.registerOutParameter(8, Types.BIGINT);
            callableSt.executeUpdate();
            return (Long) callableSt.getObject(8);
        } catch (SQLException e) {
            LOG.error("An error occurred while inserting or updating sms notification details, Cause :" + e.toString());
            throw new DBException(e, ExceptionConstant.RECORD_INSERT_FAILED);
        } finally {
            try {
                if (callableSt != null) {
                    callableSt.close();
                }
            } catch (SQLException e) {
                LOG.error("An error occurred while closing the CallableStatement, Cause :" + e.toString());
            }
            DataSourceUtils.releaseConnection(connection, jdbcTemplate.getDataSource());
            if (LOG.isInfoEnabled()) {
                LOG.info("Time taken for inserting or updating sms notification details : "
                        + ((double) (System.currentTimeMillis() - startTime) / 1000) + " seconds");
            }
        }
    }

    @Override
    @Transactional
    public Boolean insertOrUpdate_V2(SMSNotificationEntityV2 smsNotificationEntityV2) throws DBException {

        Long startTime = System.currentTimeMillis();
        Connection connection = null;
        CallableStatement callableSt = null;
        try {
            // Get Connection from dataSource
            connection = DataSourceUtils.getConnection(jdbcTemplate.getDataSource());
            callableSt = connection.prepareCall(CommonDAOConstant.StoredProcedure.INSERT_SMS_NOTIFICATION_V2);
            LOG.info("insertOrUpdate_V2-DAO- getNotificationRequestId()*************" + smsNotificationEntityV2.getNotificationRequestId());
            callableSt.setObject(1, smsNotificationEntityV2.getNotificationRequestId(), Types.BIGINT);
            callableSt.setObject(2, smsNotificationEntityV2.getMessage(), Types.VARCHAR);
            final Array mobileNumAry = connection.createArrayOf("VARCHAR", smsNotificationEntityV2.getMobileNum());
            callableSt.setObject(3, mobileNumAry, Types.ARRAY);
            final Array messageTokenAry = connection.createArrayOf("VARCHAR", smsNotificationEntityV2.getMessageToken());
            callableSt.setObject(4, messageTokenAry, Types.ARRAY);
            callableSt.registerOutParameter(5, Types.BOOLEAN);
            callableSt.executeUpdate();
            return (Boolean) callableSt.getObject(5);
        } catch (SQLException e) {
            LOG.error("An error occurred while inserting or updating sms notification details, Cause :" + e.toString());
            throw new DBException(e, ExceptionConstant.RECORD_INSERT_FAILED);
        } finally {
            try {
                if (callableSt != null) {
                    callableSt.close();
                }
            } catch (SQLException e) {
                LOG.error("An error occurred while closing the CallableStatement, Cause :" + e.toString());
            }
            DataSourceUtils.releaseConnection(connection, jdbcTemplate.getDataSource());
            if (LOG.isInfoEnabled()) {
                LOG.info("Time taken for inserting or updating sms notification details : "
                        + ((double) (System.currentTimeMillis() - startTime) / 1000) + " seconds");
            }
        }
    }

    @Override
    public Long updateMsgStatus(String messageId, Byte status, String deliverySummary) {

        Long startTime = System.currentTimeMillis();
        Connection connection = null;
        CallableStatement callableSt = null;
        try {

            // Get Connection from dataSource
            connection = DataSourceUtils.getConnection(jdbcTemplate.getDataSource());
            callableSt = connection.prepareCall(CommonDAOConstant.StoredProcedure.UPDATE_SMS_STATUS);
            callableSt.setObject(1, messageId, Types.VARCHAR);
            callableSt.setObject(2, status, Types.SMALLINT);
            callableSt.setObject(3, deliverySummary, Types.VARCHAR);
            callableSt.registerOutParameter(4, Types.BIGINT);
            callableSt.executeUpdate();
            return (Long) callableSt.getObject(4);
        } catch (SQLException e) {
            LOG.error("An error occurred while updating sms notification status, Cause :" + e.toString());
            throw new DBException(e, ExceptionConstant.RECORD_INSERT_FAILED);
        } finally {
            try {
                if (callableSt != null) {
                    callableSt.close();
                }
            } catch (SQLException e) {
                LOG.error("An error occurred while closing the CallableStatement, Cause :" + e.toString());
            }
            DataSourceUtils.releaseConnection(connection, jdbcTemplate.getDataSource());
            if (LOG.isInfoEnabled()) {
                LOG.info("Time taken for updating sms status details : " + ((double) (System.currentTimeMillis() - startTime) / 1000) + " seconds");
            }
        }
    }
}
