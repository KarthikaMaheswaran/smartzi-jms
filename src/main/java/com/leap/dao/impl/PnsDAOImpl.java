package com.leap.dao.impl;

import com.leap.dao.PnsDAO;
import com.leap.dao.constant.CommonDAOConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.leap.entity.PushNotificationEntity;
import com.leap.entity.PushNotificationEntityV2;
import com.leap.exception.DBException;
import com.leap.exception.constant.ExceptionConstant;

import java.sql.*;

@Repository
public class PnsDAOImpl implements PnsDAO {
    /**
     *
     */
    private final Logger LOG = LoggerFactory.getLogger(this.getClass());
    /**
     *
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     *
     */
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional
    public Long insertOrUpdatePNS(PushNotificationEntity pushNotificationEntity) throws DBException {
        Long startTime = System.currentTimeMillis();
        Connection connection = null;
        CallableStatement callableSt = null;
        try {
            // Get Connection from dataSource
            connection = DataSourceUtils.getConnection(jdbcTemplate.getDataSource());
            callableSt = connection.prepareCall(CommonDAOConstant.StoredProcedure.INSERT_UPDATE_PUSH_NOTIFICATION);
            callableSt.setObject(1, pushNotificationEntity.getPushNotificationId(), Types.BIGINT);
            callableSt.setObject(2, pushNotificationEntity.getNotificationRequestId(), Types.BIGINT);
            callableSt.setObject(3, pushNotificationEntity.getAppDeviceId(), Types.BIGINT);
            callableSt.setObject(4, pushNotificationEntity.getStatus(), Types.SMALLINT);
            callableSt.setObject(5, pushNotificationEntity.getMessage(), Types.VARCHAR);
            callableSt.setObject(6, pushNotificationEntity.getMessageToken(), Types.VARCHAR);
            callableSt.setObject(7, pushNotificationEntity.getPnsType(), Types.SMALLINT);
            callableSt.registerOutParameter(8, Types.BIGINT);
            callableSt.executeUpdate();
            return (Long) callableSt.getObject(8);
        } catch (SQLException e) {
            LOG.error("An error occurred while inserting push notification detail, Cause :" + e.toString());
            throw new DBException(e, ExceptionConstant.RECORD_INSERT_FAILED);
        } finally {
            try {
                if (callableSt != null) {
                    callableSt.close();
                }
            } catch (SQLException e) {
                LOG.warn("An error occurred while closing the CallableStatement, Cause :" + e.toString());
            }
            DataSourceUtils.releaseConnection(connection, jdbcTemplate.getDataSource());
            if (LOG.isInfoEnabled()) {
                LOG.info("Time taken to insert or update push notification details : "
                        + ((double) (System.currentTimeMillis() - startTime) / 1000) + " seconds");
            }
        }
    }

    @Override
    @Transactional
    public Boolean insertOrUpdatePNS_V2(PushNotificationEntityV2 pushNotificationEntity) throws DBException {
        Long startTime = System.currentTimeMillis();
        Connection connection = null;
        CallableStatement callableSt = null;
        try {
            // Get Connection from dataSource
            connection = DataSourceUtils.getConnection(jdbcTemplate.getDataSource());
            callableSt = connection.prepareCall(CommonDAOConstant.StoredProcedure.INSERT_PUSH_NOTIFICATION_V2);
            LOG.info("insertOrUpdatePNS_V2-DAO- getNotificationRequestId()*************" + pushNotificationEntity.getNotificationRequestId());
            callableSt.setObject(1, pushNotificationEntity.getNotificationRequestId(), Types.BIGINT);
            callableSt.setObject(2, pushNotificationEntity.getMessage(), Types.VARCHAR);
            final Array driverCustomerIdArr = connection.createArrayOf("VARCHAR", pushNotificationEntity.getDriverCustomerId());
            callableSt.setObject(3, driverCustomerIdArr, Types.ARRAY);
            final Array messageTokenArr = connection.createArrayOf("VARCHAR", pushNotificationEntity.getMessageToken());
            callableSt.setObject(4, messageTokenArr, Types.ARRAY);
            final Array pnsTypeArray = connection.createArrayOf("integer", pushNotificationEntity.getPnsType());
            callableSt.setObject(5, pnsTypeArray, Types.ARRAY);
            callableSt.registerOutParameter(6, Types.BOOLEAN);
            callableSt.executeUpdate();
            return (Boolean) callableSt.getObject(6);
        } catch (SQLException e) {
            LOG.error("An error occurred while inserting push notification detail, Cause :" + e.toString());
            throw new DBException(e, ExceptionConstant.RECORD_INSERT_FAILED);
        } finally {
            try {
                if (callableSt != null) {
                    callableSt.close();
                }
            } catch (SQLException e) {
                LOG.warn("An error occurred while closing the CallableStatement, Cause :" + e.toString());
            }
            DataSourceUtils.releaseConnection(connection, jdbcTemplate.getDataSource());
            if (LOG.isInfoEnabled()) {
                LOG.info("Time taken to insert or update push notification details : "
                        + ((double) (System.currentTimeMillis() - startTime) / 1000) + " seconds");
            }
        }
    }
}

