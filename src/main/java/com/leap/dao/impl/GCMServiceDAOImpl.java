package com.leap.dao.impl;


import com.google.gson.Gson;
import com.leap.dao.constant.CommonDAOConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.leap.common.constant.LogMessage;
import com.leap.dao.GCMServiceDAO;
import com.leap.jms.dto.request.DeleteInvalidDeviceTokenRequest;
import com.leap.jms.dto.response.DeleteInvalidDeviceTokenResponse;
import com.leap.exception.DBException;
import com.leap.exception.constant.ExceptionConstant;

import java.sql.*;

/**
 * @author Hisoban
 * @date 04-Aug-20
 */

@Repository
public class GCMServiceDAOImpl implements GCMServiceDAO {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional
    public DeleteInvalidDeviceTokenResponse deleteInvalidDeviceToken(DeleteInvalidDeviceTokenRequest deleteInvalidDeviceTokenReq) throws DBException {

        Gson gson = new Gson();
        LOG.info("Inside deleteInvalidDeviceToken*******" + gson.toJson(deleteInvalidDeviceTokenReq));
        Long startTime = System.currentTimeMillis();
        Connection connection = null;
        DeleteInvalidDeviceTokenResponse deleteInvalidDeviceTokenRes = null;
        CallableStatement callableSt = null;
        ResultSet resultSet = null;
        try {
            // Get Connection from dataSource
            connection = DataSourceUtils.getConnection(jdbcTemplate.getDataSource());
            callableSt = connection.prepareCall(CommonDAOConstant.StoredProcedure.DELETE_INVALID_DEVICE_TOKEN);
            callableSt.setObject(1, deleteInvalidDeviceTokenReq.getDeviceRegistrationToken(), Types.VARCHAR);
            callableSt.setObject(2, deleteInvalidDeviceTokenReq.getAppPlatformId(), Types.SMALLINT);
            callableSt.execute();
            resultSet = callableSt.getResultSet();
            if (resultSet != null) {
                if (resultSet.next()) {
                    deleteInvalidDeviceTokenRes = new DeleteInvalidDeviceTokenResponse();
                    deleteInvalidDeviceTokenRes.setMsg(resultSet.getString("Msg"));
                    deleteInvalidDeviceTokenRes.setRes(resultSet.getBoolean("Res"));
                    deleteInvalidDeviceTokenRes.setStatusCode(resultSet.getInt("StatusCode"));
                }
            }
            return deleteInvalidDeviceTokenRes;
        } catch (SQLException e) {
            LOG.error("An error occurred while delete invalid device token, Cause :" + e.toString());
            throw new DBException(e, ExceptionConstant.RECORD_FETCH_FAILED);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (callableSt != null) {
                    callableSt.close();
                }
            } catch (SQLException e) {
                LOG.warn(LogMessage.ERROR_OCCURRED_WHILE_CLOSING_CALLABLE_STATEMENT_OR_RESULT_SET + e.toString());
            }
            DataSourceUtils.releaseConnection(connection, jdbcTemplate.getDataSource());
            if (LOG.isInfoEnabled()) {
                LOG.info("Time taken for delete invalid device token : " + ((double) (System.currentTimeMillis() - startTime) / 1000) + LogMessage.SECONDS);
            }
        }
    }
}
