package com.leap.dao.impl;


import com.leap.dao.NotificationRequestDAO;
import com.leap.dao.constant.CommonDAOConstant;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.leap.entity.NotificationReqEntity;
import com.leap.exception.DBException;
import com.leap.exception.constant.ExceptionConstant;
import com.leap.common.constant.LogMessage;


import java.sql.*;

@Repository
public class NotificationRequestDAOImpl implements NotificationRequestDAO {
    /**
     *
     */
    private final org.slf4j.Logger LOG = LoggerFactory.getLogger(this.getClass());

    /**
     *
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;


    /**
     *
     */
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW) // - commented 0n 31st-july-2020
    public Long insertOrUpdate(NotificationReqEntity notificationRequestEntity) throws DBException {
        Long startTime = System.currentTimeMillis();
        Connection connection = null;
        CallableStatement callableSt = null;
        try {
            // Get Connection from dataSource
            connection = DataSourceUtils.getConnection(jdbcTemplate.getDataSource());
            callableSt = connection.prepareCall(CommonDAOConstant.StoredProcedure.INSERT_OR_UPDATE_NOTIFICATION_REQUEST);
            callableSt.setObject(1, notificationRequestEntity.getNotificationRequestId(), Types.BIGINT);
            callableSt.setObject(2, notificationRequestEntity.getNotificationRequestGUID(), Types.VARCHAR);
            callableSt.setObject(3, notificationRequestEntity.getRequestPayLoad(), Types.VARCHAR);
            callableSt.setObject(4, notificationRequestEntity.getStatus(), Types.SMALLINT);
            callableSt.setObject(5, notificationRequestEntity.getAppInfoId(), Types.SMALLINT);
            callableSt.registerOutParameter(6, Types.BIGINT);
            callableSt.executeUpdate();
            return (Long) callableSt.getObject(6);
        } catch (SQLException e) {
            LOG.error("An error occurred while inserting or updating notification request details, Cause :" + e.toString());
            throw new DBException(e, ExceptionConstant.RECORD_INSERT_FAILED);
        } finally {

            try {
                if (callableSt != null) {
                    callableSt.close();
                }
            } catch (SQLException e) {
                LOG.warn(LogMessage.ERROR_OCCURRED_WHILE_CLOSING_CALLABLE_STATEMENT_OR_RESULT_SET + e.toString());
            }
            DataSourceUtils.releaseConnection(connection, jdbcTemplate.getDataSource());
            if (LOG.isInfoEnabled()) {
                LOG.info("Time taken for inserting or updating notification request details : "
                        + ((double) (System.currentTimeMillis() - startTime) / 1000) + LogMessage.SECONDS);
            }
        }
    }

    @Override
    public NotificationReqEntity getNotificaitonRequest(String notificationRequestGUID) throws DBException {
        Long startTime = System.currentTimeMillis();
        Connection connection = null;
        CallableStatement callableSt = null;
        NotificationReqEntity notificationReqEntity = null;
        ResultSet resultSet = null;
        try {
            // Get Connection from dataSource
            connection = DataSourceUtils.getConnection(jdbcTemplate.getDataSource());
            callableSt = connection.prepareCall(CommonDAOConstant.StoredProcedure.GET_NOTIFICATION_REQUEST);
            callableSt.setObject(1, notificationRequestGUID, Types.VARCHAR);
            callableSt.executeQuery();
            resultSet = callableSt.getResultSet();
            if (resultSet != null) {
                while (resultSet.next()) {
                    notificationReqEntity = new NotificationReqEntity();
                    notificationReqEntity.setNotificationRequestId(resultSet.getLong(1));
                    notificationReqEntity.setNotificationRequestGUID(resultSet.getString(2));
                    notificationReqEntity.setRequestPayLoad(resultSet.getString(3));
                    notificationReqEntity.setStatus(resultSet.getShort(4));
                    notificationReqEntity.setAppInfoId(resultSet.getShort(5));
                }
            }
            return notificationReqEntity;
        } catch (SQLException e) {
            LOG.error("An error occurred while fetching notification request detail, Cause :" + e.toString());
            throw new DBException(e, ExceptionConstant.RECORD_FETCH_FAILED);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (callableSt != null) {
                    callableSt.close();
                }
            } catch (SQLException e) {
                LOG.warn(LogMessage.ERROR_OCCURRED_WHILE_CLOSING_CALLABLE_STATEMENT_OR_RESULT_SET + e.toString());
            }
            DataSourceUtils.releaseConnection(connection, jdbcTemplate.getDataSource());
            if (LOG.isInfoEnabled()) {
                LOG.info("Time taken for fetching notification request details : "
                        + ((double) (System.currentTimeMillis() - startTime) / 1000) + LogMessage.SECONDS);
            }
        }
    }

    @Override
    @Transactional
    public Long updateNotificationRequestStatus(String notificationRequestGUID, int status) throws DBException {
        Long startTime = System.currentTimeMillis();
        Connection connection = null;
        CallableStatement callableSt = null;
        try {
            // Get Connection from dataSource
            startTime = System.currentTimeMillis();
            connection = DataSourceUtils.getConnection(jdbcTemplate.getDataSource());
            callableSt = connection.prepareCall(CommonDAOConstant.StoredProcedure.UPDATE_NOTIFICATION_REQUEST_STATUS);
            callableSt.setObject(1, notificationRequestGUID, Types.VARCHAR);
            callableSt.setObject(2, status, Types.SMALLINT);
            callableSt.registerOutParameter(3, Types.BIGINT);
            callableSt.executeUpdate();
            return (Long) callableSt.getObject(3);
        } catch (SQLException e) {
            LOG.error("An error occurred while updating notification request status, Cause :" + e.toString());
            throw new DBException(e, ExceptionConstant.RECORD_FETCH_FAILED);
        } finally {
            try {
                if (callableSt != null) {
                    callableSt.close();
                }
            } catch (SQLException e) {
                LOG.warn(LogMessage.ERROR_OCCURRED_WHILE_CLOSING_CALLABLE_STATEMENT_OR_RESULT_SET + e.toString());
            }
            DataSourceUtils.releaseConnection(connection, jdbcTemplate.getDataSource());
            if (LOG.isInfoEnabled()) {
                LOG.info("Time taken for updating notification request status : "
                        + ((double) (System.currentTimeMillis() - startTime) / 1000) + LogMessage.SECONDS);
            }
        }
    }
}
