package com.leap.dao.impl;
import com.leap.dao.EmailNotificationDAO;
import com.leap.dao.constant.CommonDAOConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.leap.entity.EmailNotificationEntityV2;
import com.leap.exception.DBException;
import com.leap.exception.constant.ExceptionConstant;

import java.sql.*;

/**
 * @author Hisoban
 * @date 16-Jul-20
 */

@Repository
public class EmailNotificationDAOImpl implements EmailNotificationDAO {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional
    public Boolean insertEmailNotification(EmailNotificationEntityV2 emailNotificationEntity) throws DBException {
        Long startTime = System.currentTimeMillis();
        Connection connection = null;
        CallableStatement callableSt = null;
        try {

            // Get Connection from dataSource
            connection = DataSourceUtils.getConnection(jdbcTemplate.getDataSource());
            callableSt = connection.prepareCall(CommonDAOConstant.StoredProcedure.INSERT_EMAIL_NOTIFICATION_V2);
            LOG.info("insertEmailNotification-DAO- getNotificationRequestId()*************" + emailNotificationEntity.getNotificationRequestId());
            callableSt.setObject(1, emailNotificationEntity.getNotificationRequestId(), Types.BIGINT);
            callableSt.setObject(2, emailNotificationEntity.getMessage(), Types.VARCHAR);
            final Array emailIdAry = connection.createArrayOf("VARCHAR",emailNotificationEntity.getEmailId());
            callableSt.setObject(3, emailIdAry, Types.ARRAY);
            callableSt.registerOutParameter(4, Types.BOOLEAN);
            callableSt.executeUpdate();
            return (Boolean) callableSt.getObject(4);
        } catch (SQLException e) {
            LOG.error("An error occurred while inserting or updating sms notification details, Cause :" + e.toString());
            throw new DBException(e, ExceptionConstant.RECORD_INSERT_FAILED);
        } finally {
            try {
                if (callableSt != null) {
                    callableSt.close();
                }
            } catch (SQLException e) {
                LOG.error("An error occurred while closing the CallableStatement, Cause :" + e.toString());
            }
            DataSourceUtils.releaseConnection(connection, jdbcTemplate.getDataSource());
            if (LOG.isInfoEnabled()) {
                LOG.info("Time taken for inserting or updating sms notification details : "
                        + ((double) (System.currentTimeMillis() - startTime) / 1000) + " seconds");
            }
        }
    }
}


