package com.leap.dao;

import com.leap.entity.PushNotificationEntity;
import com.leap.entity.PushNotificationEntityV2;
import com.leap.exception.DBException;

/**
 * DataBase operations related to Push Notification for the application
 */
public interface PnsDAO {
    /**
     * Database operations related to push notification insert or update
     *
     * @param pushNotificationEntity pushNotificationEntity
     * @return long
     * @throws DBException DBException
     */
    Long insertOrUpdatePNS(PushNotificationEntity pushNotificationEntity) throws DBException;

    Boolean insertOrUpdatePNS_V2(PushNotificationEntityV2 pushNotificationEntity) throws DBException;
}