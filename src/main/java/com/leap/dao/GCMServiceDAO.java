package com.leap.dao;


import com.leap.jms.dto.request.DeleteInvalidDeviceTokenRequest;
import com.leap.jms.dto.response.DeleteInvalidDeviceTokenResponse;
import com.leap.exception.DBException;

/**
 * @author Hisoban
 * @date 04-Aug-20
 */
public interface GCMServiceDAO {

    /**
     * Database operation regarding delete invalid device token
     *
     * @param deleteInvalidDeviceTokenReq
     * @return long
     * @throws DBException DBException
     */
    DeleteInvalidDeviceTokenResponse deleteInvalidDeviceToken(DeleteInvalidDeviceTokenRequest deleteInvalidDeviceTokenReq) throws DBException;
}

