package com.leap.dao.constant;

public class WebhookDAOConstant {
    public interface StoredProcedure {
        String RPT_GET_B2B_BOOKING = "{call rpt_get_b2b_booking(?,?,?)}";
        String GET_COM_BOOKING_TRIP_STATUS = "{ call public.com_book_get_trip_status(?)}";
    }
}
