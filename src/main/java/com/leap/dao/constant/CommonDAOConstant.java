package com.leap.dao.constant;

/**
 * Copyright (C) MiA - Chalico Ltd.
 * Confidential and proprietary.
 */

/**
 * All DAO specific constants which are common to go here
 */
public class CommonDAOConstant {
    /**
     *
     */
    public interface StoredProcedure {
        /**
         *
         */
        String GET_USER_DETAIL_FOR_USER_ID = "{ call get_userdetail_for_userid(?) }";
        /**
         *
         */
        String GET_NOTIFICATION = "{?= call getnotificationrequestforrequestguid(?) }";
        /**
         *
         */
        String GET_PLATFORM_VERSION = "{ ?= call get_platform_version(?, ?)}";
        /**
         *
         */
        String GET_LANGUAGE = "{ call get_language(?)}";
        /**
         *
         */
        String GET_USER_OTP_DETAIL = "{ call get_user_otp(?)}";
        /**
         *
         */
        String GET_USER_DETAIL_FOR_EMAIL_ID = "{ call get_userdetail_for_email_id(?) }";
        /**
         *
         */
        String GET_NOTIFICATION_REQUEST = "{ call get_notification_request_for_request_guid(?) }";
        /**
         *
         */
        String GET_USER_DETAIL_FOR_USER_GUID = "{ call get_userdetail_for_userguid(?) }";
        /**
         *
         */
        String GET_SEQUENCE_VALUE = "{ call get_sequence_value(?) }";
        /**
         *
         */
        String GET_FORGOT_PASSWORD_BY_GUID = "{ call get_forgot_password_by_guid(?)}";
        /**
         *
         */
        String GET_FORGOT_PASSWORD_BY_USERID = "{ call get_forgot_password_by_userid(?) }";
        /**
         *
         */
        String GET_USER_DETAIL_FOR_TRIP_RESERVATION = "{ call get_user_detail_for_trip_reservation(?)}";
        /**
         *
         */
        String GET_USER_DETAIL_FOR_TRIP = "{ call get_user_detail_for_trip(?)}";
        /**
         *
         */
        String VALIDATE_USER_OTP = "{ ?= call validate_otp(?, ?)}";
        /**
         *
         */
        String UPDATE_CRD_FOR_USER = "{?= call update_password(?,?)}";
        /**
         *
         */
        String UPDATE_NOTIFICATION_REQUEST_STATUS = "{?= call update_notification_request_status(?,?)}";
        /**
         *
         */
        String DELETE_OTP = "{call delete_otp(?)}";
        /**
         *
         */
        String UPDATE_USER_STATUS = "{?= call update_user_status(?,?)}";
        /**
         *
         */
        String UPDATE_USER_CUSTOMER_NAME = "{?= call update_user_name_fromCP(?,?,?)}";
        /**
         *
         */
        String UPDATE_LOG_IN_TIME = "{?= call update_last_login_time(?)}";
        /**
         *
         */
        String UPDATE_SMS_STATUS = "{ ?= call update_sms_status(?,?,?) }";
        /**
         *
         */
        String UPDATE_SEQUENCE_VALUE = "{ ?= call update_sequence_value(?,?) }";

        /**
         *
         */
        String INSERT_OR_UPDATE_USER_AND_DEVICE_DETAIL_BOOKING_AGENT = "{?= call insert_update_user_detail_booking_agent(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        /**
         *
         */
        String INSERT_OR_UPDATE_USER_AND_DEVICE_DETAIL = "{?= call insert_update_user_detail(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

        String INSERT_OR_UPDATE_CPCUSTOMER_AND_DEVICE_DETAIL = "{?= call insert_update_cpCustomer_detail(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";


        /**
         *
         */
        String INSERT_OR_UPDATE_USER_OTP = "{?= call insert_update_user_otp(?,?,?,?)}";
        /**
         *
         */
        String INSERT_OR_UPDATE_NOTIFICATION_REQUEST = "{ ?= call insert_update_notification_request(?,?,?,?,?) }";
        /**
         *
         */
        String INSERT_OR_UPDATE_SMS_NOTIFICATION = "{ ?= call insert_update_sms_notification(?,?,?,?,?,?,?) }";
        /**
         *
         */
        String INSERT_OR_UPDATE_ADDRESS_DETAIL = "{?= call insert_update_address_detail(?,?,?,?,?,?,?,?)}";
        /**
         *
         */
        String INSERT_OR_UPDATE_USER_DETAIL = "{?= call insert_update_user_detail(?,?,?,?,?,?,?,?,?,?,?)}";
        /**
         *
         */
        String INSERT_OR_UPDATE_EMAIL_NOTIFICATION = "{?= call insert_update_email_notification(?,?,?,?,?)}";
        /**
         *
         */
        String INSERT_UPDATE_PUSH_NOTIFICATION = "{?= call insert_update_push_notification(?,?,?,?,?,?,?)}";

        String INSERT_PUSH_NOTIFICATION_V2 = "{?= call insert_push_notification_v2(?,?,?,?,?)}";

        String INSERT_EMAIL_NOTIFICATION_V2 = "{?= call insert_email_notification_v2(?,?,?)}";

        String INSERT_SMS_NOTIFICATION_V2 = "{?= call insert_sms_notification_v2(?,?,?,?)}";
        /**
         *
         */
        String INSERT_OR_UPDATE_FORGOT_PASSWORD = "{ call insert_update_forgot_password(?,?,?,?,?)}";
        /**
         *
         */
        String UPDATE_DEVICE_DETAIL = "{ ?=call update_device_detail(?,?,?,?,?,?,?,?,?,?)}";
        /**
         *
         */
        String APPVERSION_UPDATE = "{ call get_appdetail_for_usertype(?,?)}";

        /**
         *
         */
        String GET_USER_RATE_DETAIL = "{ call get_user_rate_detail_by_admin(?)}";

        /**
         *
         */
        String GET_VEHICLE_FARE_DETAIL = "{ call public.get_vehilce_rate_detail()}";
        /**
         *
         */
        String INSERT_CONTROLLER_AGENT_DETAIL = "{call insert_controller_agent_detail(?,?)}";
        /**
         *
         */

        //String INSERT_ACCOUNT_USES = "{?=call insert_account_uses(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

        String INSERT_ACCOUNT_USES = "{ call insert_update_account_user_v2(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        /**
         *
         */
        String GET_ACCOUNT_USES_DETAIL = "{ call get_account_uses_detail(?)}";
        /**
         *
         */
        String GET_ACCOUNT_USES_LIST_WITH_SEARCH = "{call get_account_uses_list_with_search(?,?,?)}";
        /**
         *
         */
        String GET_ACCOUNT_USES_LIST = "{call get_account_uses_list(?,?)}";
        /**
         *
         */
        String GET_ACCOUNT_NAME_LIST = "{call get_account_uses_by_accountname()}";
        /**
         *
         */
        String UPDATE_ACCOUNT_USES = "{?=call update_account_uses(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        /**
         *
         */
        String GET_DRIVER_REG_STATUS_BY_EMAIL_ID = "{ call get_driver_reg_status_by_email_id(?)}";

        String DELETE_CP_CUSTOMER_ROLE="{?=call delete_customer(?)}";

        String DELETE_CP_DRIVER_ROLE="{?=call delete_driver(?)}";

        /**
         *
         */
        String INSERT_DRIVER_IMAGE = "{?=call insert_driver_image(?,?)}";


        String INSET_AIRPORT_TRIP_GUEST_USER = "{ call insert_update_guest_trip_request(?,?,?,?,?,?,?,?,?,?,?) }";

        String UPDATE_AIRPORT_TRIP_GUEST_OTP = "{ call update_guest_otp(?,?,?) }";

        String CHECK_AIRPORT_TRIP_GUEST_OTP_VALID = "{ ? = call isvalid_guest_otp(?,?) }";

        String GET_AIRPORT_TRIP_CUSTOMER_DETAIL = "{ call get_airport_customer_detail(?) }";

        String GET_AIRPORT_TRIP_GUEST_USER_DETAIL = "{ call get_guest_trip_request_detail(?) }";

        String GET_GUEST_DETAIL= "{ call get_guest_trip_request_detail_by_reference_id(?) }";

        String GET_PHONE_CODE_BY_COUNTRY_CODE= "{ call get_phone_code_by_countrycode(?) }";

        String UPDATE_REFERENCE_NO = "{?=call update_reference_no_guest_trip_request(?,?)}";

        String DELETE_INVALID_DEVICE_TOKEN = "{ call delete_invalid_device_token(?,?)}";

        String INSERT_UPDATE_DRIVER_RATE_PROFILE_V2 = "{call public.insert_update_driver_rate_profile_v2(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

        String GET_USER_RATE_DETAIL_BY_ADMIN_V2 = "{call public.get_user_rate_detail_by_admin_v2(?)}";



    }

    /**
     *
     */
    public interface SqlState {
        /**
         *
         */
        String SQL_STATE_UNIQUE_STATE = "23505";
    }
}