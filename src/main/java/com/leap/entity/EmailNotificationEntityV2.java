package com.leap.entity;

import java.util.Date;

/**
 * @author Hisoban
 * @date 16-Jul-20
 */
public class EmailNotificationEntityV2 {

    private Long emailNotificationId;
    private Long notificationRequestId;
    private String emailId[];
    private String message;
    private short status;
    private Date createdOn;
    private Date updatedOn;

    public Long getEmailNotificationId() {
        return emailNotificationId;
    }

    public void setEmailNotificationId(Long emailNotificationId) {
        this.emailNotificationId = emailNotificationId;
    }

    public Long getNotificationRequestId() {
        return notificationRequestId;
    }

    public void setNotificationRequestId(Long notificationRequestId) {
        this.notificationRequestId = notificationRequestId;
    }

    public String[] getEmailId() {
        return emailId;
    }

    public void setEmailId(String[] emailId) {
        this.emailId = emailId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }
}
