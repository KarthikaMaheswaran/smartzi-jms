package com.leap.entity;


/**
 * Entity to hold SMS Notification details of the user
 */
public class SMSNotificationEntity {

    private Long smsNotificationId;
    private Long notificationRequestId;
    private String mobileNum;
    private String message;
    private short status;
    private String messageToken;
    private String deliverySummary;

    public Long getSmsNotificationId() {
        return smsNotificationId;
    }

    public void setSmsNotificationId(Long smsNotificationId) {
        this.smsNotificationId = smsNotificationId;
    }

    public Long getNotificationRequestId() {
        return notificationRequestId;
    }

    public void setNotificationRequestId(Long notificationRequestId) {
        this.notificationRequestId = notificationRequestId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public String getDeliverySummary() {
        return deliverySummary;
    }

    public void setDeliverySummary(String deliverySummary) {
        this.deliverySummary = deliverySummary;
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }

    public String getMessageToken() {
        return messageToken;
    }

    public void setMessageToken(String messageToken) {
        this.messageToken = messageToken;
    }
}
