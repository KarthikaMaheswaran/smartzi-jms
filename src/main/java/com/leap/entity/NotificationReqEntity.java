package com.leap.entity;

public class NotificationReqEntity {
    private Long notificationRequestId;

    private String notificationRequestGUID;

    private String requestPayLoad;

    private short status;

    private short appInfoId;

    public Long getNotificationRequestId() {
        return notificationRequestId;
    }

    public void setNotificationRequestId(Long notificationRequestId) {
        this.notificationRequestId = notificationRequestId;
    }

    public String getNotificationRequestGUID() {
        return notificationRequestGUID;
    }

    public void setNotificationRequestGUID(String notificationRequestGUID) {
        this.notificationRequestGUID = notificationRequestGUID;
    }

    public String getRequestPayLoad() {
        return requestPayLoad;
    }

    public void setRequestPayLoad(String requestPayLoad) {
        this.requestPayLoad = requestPayLoad;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public short getAppInfoId() {
        return appInfoId;
    }

    public void setAppInfoId(short appInfoId) {
        this.appInfoId = appInfoId;
    }
}
