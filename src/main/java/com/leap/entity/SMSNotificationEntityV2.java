package com.leap.entity;


/**
 * @author Hisoban
 * @date 16-Jul-20
 */
public class SMSNotificationEntityV2 {

    private Long notificationRequestId;
    private String message;
    private String mobileNum[];
    private String messageToken[];

    public Long getNotificationRequestId() {
        return notificationRequestId;
    }

    public void setNotificationRequestId(Long notificationRequestId) {
        this.notificationRequestId = notificationRequestId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String[] getMobileNum() {
        return mobileNum;
    }

    public void setMobileNum(String[] mobileNum) {
        this.mobileNum = mobileNum;
    }

    public String[] getMessageToken() {
        return messageToken;
    }

    public void setMessageToken(String[] messageToken) {
        this.messageToken = messageToken;
    }
}
