package com.leap.entity;


import java.util.Date;

/**
 * Entity to hold push notification related data
 */
public class PushNotificationEntity {

    private Long pushNotificationId;
    private Long notificationRequestId;
    private Long appDeviceId;
    private short status;
    private String message;
    private String messageToken;
    private Integer pnsType;
    private Date createdOn;
    private Date updatedOn;

    public Long getPushNotificationId() {
        return pushNotificationId;
    }

    public void setPushNotificationId(Long pushNotificationId) {
        this.pushNotificationId = pushNotificationId;
    }

    public Long getNotificationRequestId() {
        return notificationRequestId;
    }

    public void setNotificationRequestId(Long notificationRequestId) {
        this.notificationRequestId = notificationRequestId;
    }

    public Long getAppDeviceId() {
        return appDeviceId;
    }

    public void setAppDeviceId(Long appDeviceId) {
        this.appDeviceId = appDeviceId;
    }

    public short getStatus() {
        return status;
    }

    public void setStatus(short status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getMessageToken() {
        return messageToken;
    }

    public void setMessageToken(String messageToken) {
        this.messageToken = messageToken;
    }

    public Integer getPnsType() {
        return pnsType;
    }

    public void setPnsType(Integer pnsType) {
        this.pnsType = pnsType;
    }
}
