package com.leap.service;

import com.leap.jms.dto.request.NotificationRequest;

import javax.jms.JMSException;

public interface JmsService {

    public void sendNotification(NotificationRequest notificationRequest) throws JMSException;
}
