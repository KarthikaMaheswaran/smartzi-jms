package com.leap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.leap.bussiness.JmsBusiness;
import com.leap.jms.dto.request.NotificationRequest;
import com.leap.service.JmsService;

import javax.jms.JMSException;

@RestController
public class JmsServiceImpl implements JmsService {

    @Autowired
    JmsBusiness jmsBusiness;

    @Override
    @PostMapping("/jms/sendMessage")
    public void sendNotification(@RequestBody NotificationRequest notificationRequest) throws JMSException {
        jmsBusiness.sendNotification(notificationRequest);
    }
}
