/**
 * Copyright (C) MiA - Chalico Ltd.
 * Confidential and proprietary.
 */
package com.leap.notification.sms;


import com.leap.exception.SMSException;
import com.leap.notification.sms.model.ClickatellMessage;
import com.leap.notification.sms.model.ClickatellSMSResponse;
import com.leap.notification.sms.model.SMSMessageDetail;
import com.leap.notification.sms.model.SMSNotification;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.leap.notification.sms.constant.SMSConstant;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * This class is used to send sms
 * 
 */
@Component
public class SMSNotificationSender {
    /**
     * 
     */
    private static final Logger LOG = Logger.getLogger(SMSNotificationSender.class);
    /**
     * 
     */
    @Value("${clickatell.senderid}")
    private String senderId;

    /**
     * 
     */
    public void send(SMSNotification smsNotification) throws SMSException {

        List<String> tos = new ArrayList<>();
        Map<String, Object> pushdata = new HashMap<>();
        Map<String, Object> messageDetailMap = new HashMap<>();
        pushdata.put(SMSConstant.ProviderParam.ClickATell.TEXT, smsNotification.getPayLoad());
        pushdata.put(SMSConstant.ProviderParam.ClickATell.FROM, senderId);
        pushdata.put("callback", 7);
        for (SMSMessageDetail messageDetail : smsNotification.getMessageDetails()) {
            tos.add(messageDetail.getRecipientNumber());
            messageDetail.setMessage(smsNotification.getPayLoad());
            // Setting default status as failed
            messageDetail.setStatus(SMSConstant.NotificationStatus.FAILED);
            messageDetailMap.put(messageDetail.getRecipientNumber(), messageDetail);
        }
        pushdata.put(SMSConstant.ProviderParam.ClickATell.TO, tos);
        try {
            LOG.debug("Going to connect to clickatell");
            URL obj = new URL(SMSConstant.Provider.CLICKATELL_ENDPOINT);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // add request header
            con.setRequestMethod("POST");
            con.setRequestProperty("Authorization",
                    "bearer " + smsNotification.getInitParams().get(SMSConstant.ProviderParam.ClickATell.BEARER_TOKEN));
            con.setRequestProperty("X-Version", "1");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");

            // Send post request
            con.setDoOutput(true);
            DataOutputStream dOutputStream = new DataOutputStream(con.getOutputStream());
            dOutputStream.writeBytes(JSONObject.toJSONString(pushdata));
            dOutputStream.flush();
            dOutputStream.close();
            if (LOG.isDebugEnabled()) {
                LOG.debug("Connection Established");
            }
            BufferedReader inputStream = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
            String inputLine;
            StringBuilder response = new StringBuilder();
            if (LOG.isDebugEnabled()) {
                LOG.debug("Getting Response");
            }
            while ((inputLine = inputStream.readLine()) != null) {
                response.append(inputLine);
            }
            inputStream.close();
            if (LOG.isInfoEnabled()) {
                LOG.info(response.toString());
            }
            ObjectMapper objectMapper = new ObjectMapper();
            if (LOG.isDebugEnabled()) {
                LOG.debug("Going to fetch message Id from response and set to message token");
            }
            ClickatellSMSResponse clickatellSMSResponse = objectMapper.readValue(response.toString(),
                    ClickatellSMSResponse.class);
            SMSMessageDetail messageDetail = null;
            String recipient = null;
            for (ClickatellMessage clickatellMessage : clickatellSMSResponse.getClickatellSMSData()
                    .getClickatellMessages()) {
                recipient = clickatellMessage.getTo();
                if (!recipient.startsWith("+")) {
                    recipient = "+" + recipient;
                }
                messageDetail = (SMSMessageDetail) messageDetailMap.get(recipient);
                messageDetail.setMessageToken(clickatellMessage.getApiMessageId());
                // Updating status as sent once received response from Cickatell
                messageDetail.setStatus(SMSConstant.NotificationStatus.SENT);
            }
        } catch (IOException e) {
            LOG.error("An error occurred while trying to send sms notification , Cause: " + e.toString());
        }
    }

}
