/**
 * Copyright (C) MiA - Chalico Ltd.
 * Confidential and proprietary.
 */
package com.leap.notification.sms.constant;

/**
 * 
 * This class contains clickatell sms status
 * 
 */
public enum ClickatellSMSStatusEnum {
    _001((byte) 4, "001", "Message_unknown"), _002((byte) 1, "002", "Message_queued"), _003((byte) 1, "003",
            "Delivered_to_gateway"), _004((byte) 2, "004", "Received_by_recipient"), _005((byte) 4, "005",
            "Error_with_message"), _006((byte) 2, "006", "User_cancelled_message_delivery"), _007((byte) 4, "007",
            "Error_delivering_message"), _009((byte) 4, "009", "Routing_error"), _010((byte) 4, "010", "Message_expired"), _011(
            (byte) 1, "011", "Message_scheduled_for_later_delivery"), _012((byte) 4, "012", "Out_of_credit"), _013(
            (byte) 4, "013", "Clickatell_cancelled_message_delivery"), _014((byte) 4, "014", "Maximum_MT_limit_exceeded");
    /**
     * 
     */
    private final Byte id;
    /**
     * 
     */
    private final String status;
    /**
     * 
     */
    private final String deliverySummary;

    /**
     * 
     */
    private ClickatellSMSStatusEnum(Byte id, String status, String deliverySummary) {
        this.id = id;
        this.status = status;
        this.deliverySummary = deliverySummary;
    }

    /**
     * 
     */
    public Byte getId() {
        return id;
    }

    /**
     * 
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     */
    public String getDeliverySummary() {
        return deliverySummary;
    }
}
