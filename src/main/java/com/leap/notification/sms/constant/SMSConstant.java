package com.leap.notification.sms.constant;

/**
 * All sms related constants to go here
 */
public class SMSConstant {
    /**
     *
     */
    public interface NotificationStatus {
        /**
         *
         */
        Byte FAILED = 4;
        /**
         *
         */
        Byte SENT = 1;
        /**
         *
         */
        Byte UNSENT = 0;
        /**
         *
         */
        Byte RETRYING = 5;
        /**
         *
         */
        Byte ACKNOWLEDGED = 2;
        /**
         *
         */
        Byte INACTIVE_DEVICE_TOKEN = 6;
    }

    /**
     *
     */
    public interface Provider {
        /**
         *
         */
        String CLICKATELL = "ClickATell";
        /**
         *
         */
        String CLICKATELL_ENDPOINT = "https://api.clickatell.com/rest/message";
    }

    /**
     * Parameter constants specific to SMS Provider
     */
    public interface ProviderParam {
        /**
         *
         */
        interface ClickATell {
            /**
             *
             */
            String BEARER_TOKEN = "BEARER_TOKEN";
            /**
             *
             */
            String TO = "to";
            /**
             *
             */
            String TEXT = "text";
            /**
             *
             */
            String FROM = "from";
        }
    }
}
