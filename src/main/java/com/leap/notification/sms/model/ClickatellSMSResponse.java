/**
 * Copyright (C) MiA - Chalico Ltd.
 * Confidential and proprietary.
 */
package com.leap.notification.sms.model;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * This class contain clickatell message response data
 * 
 */
public class ClickatellSMSResponse {
    /**
     * 
     */
    @JsonProperty("data")
    private ClickatellSMSData clickatellSMSData;

    /**
     * 
     */
    public ClickatellSMSData getClickatellSMSData() {
        return clickatellSMSData;
    }

    /**
     * 
     */
    public void setClickatellSMSData(ClickatellSMSData clickatellSMSData) {
        this.clickatellSMSData = clickatellSMSData;
    }

}
