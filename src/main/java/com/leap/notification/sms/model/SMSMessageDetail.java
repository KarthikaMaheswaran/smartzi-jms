package com.leap.notification.sms.model;

/**
 * 
 * Model to hold SMS recipient mobile number and status
 * 
 */
public class SMSMessageDetail {

    private String message;
    private String messageToken;
    private String recipientNumber;
    private Byte status = 0; //Status of message. possible value 0 - Failed, 1 - Sent

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the messageToken
     */
    public String getMessageToken() {
        return messageToken;
    }

    /**
     * @param messageToken
     *            the messageToken to set
     */
    public void setMessageToken(String messageToken) {
        this.messageToken = messageToken;
    }

    /**
     * @return the recipientNumber
     */
    public String getRecipientNumber() {
        return recipientNumber;
    }

    /**
     * @param recipientNumber
     *            the recipientNumber to set
     */
    public void setRecipientNumber(String recipientNumber) {
        this.recipientNumber = recipientNumber;
    }

    /**
     * @return the status
     */
    public Byte getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(Byte status) {
        this.status = status;
    }

}
