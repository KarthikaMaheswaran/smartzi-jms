/**
 * Copyright (C) MiA - Chalico Ltd.
 * Confidential and proprietary.
 */
package com.leap.notification.sms.model;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * This class contain clickatell message
 * 
 */
public class ClickatellSMSData {
    /**
     * 
     */
    @JsonProperty("message")
    private List<ClickatellMessage> clickatellMessages;

    /**
     * 
     */
    public List<ClickatellMessage> getClickatellMessages() {
        return clickatellMessages;
    }

    /**
     * 
     */
    public void setClickatellMessages(List<ClickatellMessage> clickatellMessages) {
        this.clickatellMessages = clickatellMessages;
    }
}
