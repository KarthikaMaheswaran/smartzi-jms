/**
 * Copyright (C) MiA - Chalico Ltd.
 * Confidential and proprietary.
 */
package com.leap.notification.sms.model;

/**
 * This class contain clickatell message related data
 * 
 */
public class ClickatellMessage {
    /**
     * 
     */
    private String accepted;
    /**
     * 
     */
    private String to;
    /**
     * 
     */
    private String apiMessageId;

    /**
     * 
     */
    public String getAccepted() {
        return accepted;
    }

    /**
     * 
     */
    public void setAccepted(String accepted) {
        this.accepted = accepted;
    }

    /**
     * 
     */
    public String getTo() {
        return to;
    }

    /**
     * 
     */
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * 
     */
    public String getApiMessageId() {
        return apiMessageId;
    }

    /**
     * 
     */
    public void setApiMessageId(String apiMessageId) {
        this.apiMessageId = apiMessageId;
    }

}
