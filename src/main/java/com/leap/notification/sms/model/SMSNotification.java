/**
 * Copyright (C) MiA - Chalico Ltd.
 * Confidential and proprietary.
 */
package com.leap.notification.sms.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class contain sms notification related data
 * 
 */
public class SMSNotification {

    /**
     * Map of parameters which may include header parameters or application
     * specific certificate etc. required for respective PNS
     */
    private Map<String, Object> initParams = new HashMap<>();

    /**
     * Contains List of device tokens and maintains respective device delivery
     * status
     */
    private List<SMSMessageDetail> messageDetails = new ArrayList<>();

    /**
     * The message payload to be pushed to the respective PNS
     */
    private String payLoad;

    public Map<String, Object> getInitParams() {
        return initParams;
    }

    public void setInitParams(Map<String, Object> initParams) {
        this.initParams = initParams;
    }

    public String getPayLoad() {
        return payLoad;
    }

    public void setPayLoad(String payLoad) {
        this.payLoad = payLoad;
    }

    public List<SMSMessageDetail> getMessageDetails() {
        return messageDetails;
    }

    public void setMessageDetails(List<SMSMessageDetail> messageDetails) {
        this.messageDetails = messageDetails;
    }

}
