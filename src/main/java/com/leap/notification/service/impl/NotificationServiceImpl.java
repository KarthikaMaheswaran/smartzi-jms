package com.leap.notification.service.impl;

import com.google.gson.Gson;


import com.leap.NotificationConfig;
import com.leap.common.constant.AppPlatformEnum;
import com.leap.dao.EmailNotificationDAO;
import com.leap.dao.PnsDAO;
import com.leap.dao.SMSNotificationDAO;
import com.leap.jms.dto.DeviceInfoDTO;
import com.leap.notification.model.*;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Component;
import com.leap.common.constant.AppTypeEnum;
import com.leap.common.constant.LogMessage;
import com.leap.entity.EmailNotificationEntityV2;
import com.leap.entity.PushNotificationEntity;
import com.leap.entity.PushNotificationEntityV2;
import com.leap.entity.SMSNotificationEntityV2;
import com.leap.exception.DBException;
import com.leap.exception.NotificationException;


import com.leap.exception.SMSException;
import com.leap.notification.mail.MailNotificationSender;
import com.leap.notification.mail.model.MailMessageDetail;
import com.leap.notification.mail.model.SMTPConfiguration;
import com.leap.notification.pns.constant.PNSConstant;
import com.leap.notification.pns.model.PNSMessageDetail;
import com.leap.notification.pns.model.PushNotification;
import com.leap.notification.service.NotificationService;
import com.leap.notification.sms.SMSNotificationSender;
import com.leap.notification.pns.service.impl.GCMService;
import com.leap.notification.pns.service.impl.APNSService;
import com.leap.notification.sms.constant.SMSConstant;
import com.leap.notification.sms.model.SMSMessageDetail;
import com.leap.notification.sms.model.SMSNotification;


import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

@Component
public class NotificationServiceImpl implements NotificationService {

    private static final Logger LOG = Logger.getLogger(NotificationServiceImpl.class);

    @Autowired
    private SMSNotificationDAO smsNotificationDAO;

    @Autowired
    EmailNotificationDAO emailNotificationDAO;

    @Autowired
    private PnsDAO pnsdao;


    @Autowired
    private SMTPConfiguration smtpConfiguration;


//    @Autowired(required = true)
//    private VelocityEngine velocityEngine;


    @Autowired
    private Properties appConfig;


    @Value("${notification.sms.enable}")
    private Boolean notificationSMSEnable;

    @Value("${notification.email.enable}")
    private Boolean notificationEmailEnable;

    @Value("${notification.push.enable}")
    private Boolean notificationPushEnable;

    @Value("${leap.driver.p12.usetest}")
    private Boolean driverP12UseTest;

    @Value("${leap.customer.p12.usetest}")
    private Boolean customerP12UseTest;

    @Autowired
    private SMSNotificationSender smsNotificationSender;

    @Autowired
    private GCMService GCMService;

    @Autowired
    private APNSService APNSService;


    ApplicationContext applicationContext = new AnnotationConfigApplicationContext(NotificationConfig.class);

    @Autowired
    VelocityEngine velocityEngine;

//    VelocityEngineFactoryBean velocityEngine= (VelocityEngineFactoryBean) applicationContext.getBean("velocityEngine");

    /**
     *
     */
    @SuppressWarnings("unchecked")
    @Override
    public void sendMessage(NotificationRequest notificationRequest, Long notificationRequestId) throws NotificationException {

        LOG.info("Inside Notification sendMessage***********");
        if (notificationRequest == null) {
            throw new NotificationException("test-Notification request is null");
        }


        if (notificationRequest.getTemplateID() == null || notificationRequest.getTemplateID().isEmpty()) {
            throw new NotificationException("test-Template ID is null");
        }

        Map<String, String> notificationTemplateMap = (HashMap<String, String>) applicationContext.getBean(notificationRequest.getTemplateID());
        if (notificationTemplateMap == null) {
            throw new NotificationException("test-Notification template not found");
        }

        String templateName = null;
        int sendCount = 0;
        /* Email Notification starts here */
        EmailMessage emailMessage = notificationRequest.getEmailMessage();
        if (emailMessage != null) {
            templateName = notificationTemplateMap.get("email");
            if (notificationEmailEnable) {
                try {
                    sendEmail(emailMessage, templateName, notificationRequestId);
                    sendCount++;
                } catch (NotificationException e) {
                    LOG.warn("test-An error occured while sending email notification. Cause " + e.toString());
                }
            }
        }

        /* SMS Notification starts here */
        SMSMessage smsMessage = notificationRequest.getSmsMessage();
        if (smsMessage != null) {
            templateName = notificationTemplateMap.get("sms");
            if (notificationSMSEnable) {
                try {
                    sendSMS(smsMessage, templateName, notificationRequestId);
                    sendCount++;
                } catch (NotificationException e) {
                    LOG.warn("test-An error occured while sending sms notification. Cause " + e.toString());
                }
            }
        }

        /* PNS Notification starts here */
        PNSMessage pnsMessage = notificationRequest.getPnsMessage();
        LOG.info("*************************");
        LOG.info("PNS*******************");
        LOG.info("*************************");
        if (pnsMessage != null) {
            templateName = notificationTemplateMap.get("pns");
            if (notificationPushEnable) {
                try {
                    sendPNS(pnsMessage, templateName, notificationRequestId);
                    sendCount++;
                } catch (NotificationException e) {
                    LOG.warn("test-An error occured while sending push notification. Cause " + e.toString());
                }
            }
        }
        if (sendCount == 0) {
            throw new NotificationException("Retry", new Throwable("Notification Exception"));
        }

    }

    /**
     *
     */
    private void sendEmail(EmailMessage emailMessage, String templateName, Long notificationRequestId) throws NotificationException {
        LOG.info("test-Inside Notification sendMessage:sendEmail***********");
        validateEmail(emailMessage, templateName);

        try {
            Map<String, Object> bodyData = emailMessage.getPlaceHolderData();
            String bodyMsg = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, templateName, LogMessage.UTF, bodyData);

            String from = appConfig.getProperty("emailFromLeap");

            MailMessageDetail mailMessageDetail = new MailMessageDetail();
            mailMessageDetail.setBody(bodyMsg);
            mailMessageDetail.setFromAddress(from);
            mailMessageDetail.setSubject(emailMessage.getSubject());
            mailMessageDetail.setToAddresses(emailMessage.getToRecipients());
            mailMessageDetail.setCcAddresses(emailMessage.getCcRecipients());
            mailMessageDetail.setBccAddresses(emailMessage.getBccRecipients());
            mailMessageDetail.setContentType(Byte.valueOf("2"));
            mailMessageDetail.setAttchmentUrl(emailMessage.getAttchmentUrl());
            mailMessageDetail.setAttachment(emailMessage.isAttachment());
            mailMessageDetail.setBucketName(emailMessage.getBucketName());
            MailNotificationSender mailNotificationSender = new MailNotificationSender(smtpConfiguration, mailMessageDetail);
            mailNotificationSender.sendMessage();
            populateEmailNotification(notificationRequestId, mailMessageDetail);// added on 16-07-2020
        } catch (MailException e) {
            LOG.error("test-An error occured while sending email notification. Cause " + e.toString());
            throw new NotificationException("Retry", e);
        }
    }

    /**
     *
     */
    private void sendSMS(SMSMessage smsMessage, String templateName, Long notificationRequestId) throws NotificationException {

        LOG.info("test-Inside Notification sendMessage:sendSMS***********");
        validateSMS(smsMessage, templateName);

        try {
            Map<String, Object> bodyData = smsMessage.getPlaceHolderData();
            String bodyMsg = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, templateName, LogMessage.UTF, bodyData);

            List<SMSMessageDetail> smsMessageDetails = new ArrayList<>();
            List<String> smsRecipients = smsMessage.getToRecipients();
            for (String recipient : smsRecipients) {
                SMSMessageDetail messageDetail = new SMSMessageDetail();
                messageDetail.setRecipientNumber(recipient);
                smsMessageDetails.add(messageDetail);
            }

            String bearerToken = appConfig.getProperty("clickaTelBearerToken");
            Map<String, Object> initParams = new HashMap<>();
            initParams.put(SMSConstant.ProviderParam.ClickATell.BEARER_TOKEN, bearerToken);

            SMSNotification smsNotification = new SMSNotification();
            smsNotification.setPayLoad(bodyMsg);
            smsNotification.setMessageDetails(smsMessageDetails);
            smsNotification.setInitParams(initParams);
            smsNotificationSender.send(smsNotification);
            if (smsMessageDetails.size() > 0) {
//                for (SMSMessageDetail smsMessageDetail : smsMessageDetails) {
//                    populateSMSNotification(notificationRequestId, smsMessageDetail);
//                }
                //------ added on 16-july-2020------
                String smsMessageToken[] = new String[smsMessageDetails.size()];
                String smsMobileNum[] = new String[smsMessageDetails.size()];
                for (int i = 0; i < smsMessageDetails.size(); i++) {
                    smsMessageToken[i] = smsMessageDetails.get(i).getMessageToken();
                    smsMobileNum[i] = smsMessageDetails.get(i).getRecipientNumber();
                }
                SMSMessageDBDetail smsMessageDetailDBRes = new SMSMessageDBDetail();
                smsMessageDetailDBRes.setMessageToken(smsMessageToken);
                smsMessageDetailDBRes.setMessage(smsMessageDetails.get(0).getMessage());
                smsMessageDetailDBRes.setMobileNo(smsMobileNum);
                smsMessageDetailDBRes.setNotificationRequestId(notificationRequestId);
                populateSMSNotificationV2(smsMessageDetailDBRes);
                //-------------------
            }

        } catch (SMSException e) {
            LOG.error("test-An error occured while sending sms notification. Cause " + e.toString());
            throw new NotificationException("Retry", e);
        } catch (DBException e) {
            LOG.warn("test-An error occured while populating sms sent detail into DB. Cause " + e.toString());
        }
    }

    /**
     *
     */
//    private void populateSMSNotification(Long notificationRequestId, SMSMessageDetail smsMessageDetail) {
//        LOG.info("Inside Notification - sendMessage:populateSMSNotification*******");
//        SMSNotificationEntity smsNotificationEntity = new SMSNotificationEntity();
//        smsNotificationEntity.setMessageToken(smsMessageDetail.getMessageToken());
//        smsNotificationEntity.setMobileNum(smsMessageDetail.getRecipientNumber());
//        smsNotificationEntity.setMessage(smsMessageDetail.getMessage());
//        smsNotificationEntity.setNotificationRequestId(notificationRequestId);
//        smsNotificationEntity.setStatus(smsMessageDetail.getStatus());
//        smsNotificationDAO.insertOrUpdate(smsNotificationEntity);
//    }

    // ======added 16-july-2020=======
    private void populateSMSNotificationV2(SMSMessageDBDetail smsMessageDetailDBRes) {
        LOG.info("test-Inside Notification - sendMessage:populateSMSNotification*******");
        SMSNotificationEntityV2 smsNotificationEntyV2 = new SMSNotificationEntityV2();
        smsNotificationEntyV2.setNotificationRequestId(smsMessageDetailDBRes.getNotificationRequestId());
        smsNotificationEntyV2.setMessage(smsMessageDetailDBRes.getMessage());
        smsNotificationEntyV2.setMobileNum(smsMessageDetailDBRes.getMobileNo());
        smsNotificationEntyV2.setMessageToken(smsMessageDetailDBRes.getMessageToken());
        smsNotificationDAO.insertOrUpdate_V2(smsNotificationEntyV2);
    }

    // =====added on 17-July-2020============
    private void populateEmailNotification(Long notificationRequestId, MailMessageDetail mailMessageDetail) {
        LOG.info("test-Inside Notification - sendMessage:populateEmailNotification*******");
        EmailNotificationEntityV2 emailNotificationEntity = new EmailNotificationEntityV2();
        emailNotificationEntity.setNotificationRequestId(notificationRequestId);
        emailNotificationEntity.setMessage(mailMessageDetail.getBody());
        String toAddressArr[] = new String[mailMessageDetail.getToAddresses().size()];
        toAddressArr = mailMessageDetail.getToAddresses().toArray(toAddressArr);
        emailNotificationEntity.setEmailId(toAddressArr);
        emailNotificationDAO.insertEmailNotification(emailNotificationEntity);
    }

    /**
     *
     */
    private void sendPNS(PNSMessage pnsMessage, String templateName, Long notificationRequestId) throws NotificationException {
        Gson gson = new Gson();
        LOG.info("test-Inside Notification sendMessage:sendPNS***********");
        validatePNS(pnsMessage, templateName);
        LOG.info("-------------------------------------------------------");
        LOG.info("-------------------------------------------------------");
        LOG.info("----------------------pass validation------------------");
        LOG.info("--------------------------------------------------------");
        LOG.info("-------------------------------------------------------");

            Map<String, Object> bodyData = pnsMessage.getPlaceHolderData();
            LOG.info("----------------------bodyData------------------" + gson.toJson(bodyData));
            LOG.info("------------------------templateName----------------" + gson.toJson(templateName));
            String bodyMsg = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, templateName, LogMessage.UTF, bodyData);
            LOG.info("----------------------bodyMsg------------------" + gson.toJson(bodyMsg));


            List<PNSMessageDetail> apnsPNSMessageDetails = null;
            List<PNSMessageDetail> gcmPNSMessageDetails = null;

            List<DeviceInfoDTO> deviceInfoDTOs = pnsMessage.getDeviceInfoDTOs();
            for (DeviceInfoDTO deviceInfoDTO : deviceInfoDTOs) {
                LOG.info("----------------------deviceInfoDTO------------------" + gson.toJson(deviceInfoDTO));

                PNSMessageDetail pnsMessageDetail = new PNSMessageDetail();
                pnsMessageDetail.setDeviceRegistrationToken(deviceInfoDTO.getDeviceRegistrationToken());
                pnsMessageDetail.setPlatformType(deviceInfoDTO.getPlatformType());
                LOG.info(deviceInfoDTO.getPlatformType() + "____________PLATFORMS__________________" + deviceInfoDTO.getAppType());
                pnsMessageDetail.setInitParams(getInitParams(deviceInfoDTO.getPlatformType(), deviceInfoDTO.getAppType()));
                pnsMessageDetail.setMessage(bodyMsg);
                String messageToken = UUID.randomUUID().toString();
                pnsMessageDetail.setMessageToken(messageToken);
                if (deviceInfoDTO.getPlatformType().equals(AppPlatformEnum.ios.getId())) {
                    if (apnsPNSMessageDetails == null) {
                        apnsPNSMessageDetails = new ArrayList<>();
                    }
                    apnsPNSMessageDetails.add(pnsMessageDetail);
                } else {
                    if (gcmPNSMessageDetails == null) {
                        gcmPNSMessageDetails = new ArrayList<>();
                    }
                    gcmPNSMessageDetails.add(pnsMessageDetail);
                }
            }

            Map<String, Object> payloadparams = new HashMap<>();
            if (pnsMessage.getPayloadParams() != null) {
                payloadparams.putAll(pnsMessage.getPayloadParams());
            }

            if (apnsPNSMessageDetails != null) {
                LOG.info("###############-----------apnsPNSMessageDetails------#######################");
                PushNotification pushNotification = new PushNotification();
                pushNotification.setPayLoad(bodyMsg);
                pushNotification.setMessageDetails(apnsPNSMessageDetails);
                pushNotification.setPayloadParams(payloadparams);
//                for (PNSMessageDetail pnsMessageDetail : apnsPNSMessageDetails) {
//                    populatePNSNotification(notificationRequestId, pnsMessageDetail, pnsMessageDetail.getPlatformType());
//                }
                // ----------added on 16-July-2020------------
                LOG.info("test-Inside Notification sendMessage:sendPNS***********");
                String pnsDriverCustomerId[] = new String[apnsPNSMessageDetails.size()];
                String pnsMessageToken[] = new String[apnsPNSMessageDetails.size()];
                Integer pnsType[] = new Integer[apnsPNSMessageDetails.size()];
                for (int i = 0; i < apnsPNSMessageDetails.size(); i++) {
                    if (apnsPNSMessageDetails.get(i).getPlatformType() != null) {
                        pnsMessageToken[i] = apnsPNSMessageDetails.get(i).getMessageToken();
                        pnsDriverCustomerId[i] = "D-0";
                        pnsType[i] = Integer.valueOf(apnsPNSMessageDetails.get(i).getPlatformType());
                    }
                }
                PNSMessageDBDetail pnsMessageDBDetail = new PNSMessageDBDetail();
                pnsMessageDBDetail.setNotificationRequestId(notificationRequestId);
                pnsMessageDBDetail.setMessage(apnsPNSMessageDetails.get(0).getMessage());
                pnsMessageDBDetail.setDriverCustomerId(pnsDriverCustomerId);
                pnsMessageDBDetail.setMessageToken(pnsMessageToken);
                pnsMessageDBDetail.setPnsType(pnsType);
                populatePNSNotificationV2(pnsMessageDBDetail);

//                PushNotificationSender pushNotificationSender = new PushNotificationSender(AppPlatformEnum.ios.getId());
//                pushNotificationSender.sendMessage(pushNotification);
                APNSService.push(pushNotification);
            }

            if (gcmPNSMessageDetails != null) {
                LOG.info("###############-----------gcmPNSMessageDetails------#######################");
                PushNotification pushNotification = new PushNotification();
                pushNotification.setPayLoad(bodyMsg);
                pushNotification.setMessageDetails(gcmPNSMessageDetails);
                pushNotification.setPayloadParams(payloadparams);

//                for (PNSMessageDetail pnsMessageDetail : gcmPNSMessageDetails) {
//                    populatePNSNotification(notificationRequestId, pnsMessageDetail, pnsMessageDetail.getPlatformType());
//                }
                // ----------added on 16-July-2020------------
                String pnsDriverCustomerId[] = new String[gcmPNSMessageDetails.size()];
                String pnsMessageToken[] = new String[gcmPNSMessageDetails.size()];
                Integer pnsType[] = new Integer[gcmPNSMessageDetails.size()];
                for (int i = 0; i < gcmPNSMessageDetails.size(); i++) {
                    if (gcmPNSMessageDetails.get(i).getPlatformType() != null) {
                        pnsMessageToken[i] = gcmPNSMessageDetails.get(i).getMessageToken();
                        pnsDriverCustomerId[i] = "D-0";
                        pnsType[i] = Integer.valueOf(gcmPNSMessageDetails.get(i).getPlatformType());
                    }
                }
                PNSMessageDBDetail pnsMessageDBDetail = new PNSMessageDBDetail();
                pnsMessageDBDetail.setNotificationRequestId(notificationRequestId);
                pnsMessageDBDetail.setMessage(gcmPNSMessageDetails.get(0).getMessage());
                pnsMessageDBDetail.setDriverCustomerId(pnsDriverCustomerId);
                pnsMessageDBDetail.setMessageToken(pnsMessageToken);
                pnsMessageDBDetail.setPnsType(pnsType);
                populatePNSNotificationV2(pnsMessageDBDetail);

//                PushNotificationSender pushNotificationSender = new PushNotificationSender(AppPlatformEnum.android.getId());
//                pushNotificationSender.sendMessage(pushNotification);
                GCMService.push(pushNotification);
            }


    }

    /**
     * @param platformType platformType
     * @param appType      appType
     * @return map
     */
    private Map<String, Object> getInitParams(Short platformType, Short appType) {
        Map<String, Object> initParams = new HashMap<>();
        if (platformType.equals(AppPlatformEnum.ios.getId())) {
            if (appType.equals(AppTypeEnum.driverapp.getId())) {
                initParams.put(PNSConstant.PnsParam.CERT_PATH, appConfig.getProperty("driverP12Path"));
                initParams.put(PNSConstant.PnsParam.CERT_PD, appConfig.getProperty("driverP12Pass"));
                initParams.put(PNSConstant.PnsParam.USE_TEST, driverP12UseTest);
            } else {
                LOG.info("_____________PATH__________________" + appConfig.getProperty("customerP12Path"));
                initParams.put(PNSConstant.PnsParam.CERT_PATH, appConfig.getProperty("customerP12Path"));
                initParams.put(PNSConstant.PnsParam.CERT_PD, appConfig.getProperty("customerP12Pass"));
                initParams.put(PNSConstant.PnsParam.USE_TEST, customerP12UseTest);
            }
        } else {
            /*if (appType.equals(AppTypeEnum.driverapp.getId())) {
                initParams.put(PNSConstant.PnsParam.APP_CERTIFICATE, appConfig.getProperty("driverGCMSenderId"));
            } else {
                initParams.put(PNSConstant.PnsParam.APP_CERTIFICATE, appConfig.getProperty("customerGCMSenderId"));
            }*/

            if (appType.equals(AppTypeEnum.driverapp.getId())) {
                initParams.put(PNSConstant.PnsParam.APP_CERTIFICATE, appConfig.getProperty("driverFCMSenderId"));
                LOG.info("test-driver fcm key **************" + appConfig.getProperty("driverFCMSenderId"));
            } else {
                initParams.put(PNSConstant.PnsParam.APP_CERTIFICATE, appConfig.getProperty("customerFCMSenderId"));
                LOG.info("test-Customer fcm key **************" + appConfig.getProperty("customerFCMSenderId"));
            }

           /* initParams.put(PNSConstant.PnsParam.APP_CERTIFICATE, appConfig.getProperty("androidFCMSenderId"));
            LOG.info("driver fcm key **************" + appConfig.getProperty("androidFCMSenderId"));*/
        }
        return initParams;
    }

    /**
     *
     */
    private void populatePNSNotification(Long notificationRequestId, PNSMessageDetail pnsMessageDetail, int pnsTypeId) {
        PushNotificationEntity pushNotificationEntity = new PushNotificationEntity();
        pushNotificationEntity.setNotificationRequestId(notificationRequestId);
        pushNotificationEntity.setAppDeviceId(pnsMessageDetail.getDeviceId());
        pushNotificationEntity.setMessageToken(pnsMessageDetail.getMessageToken());
        pushNotificationEntity.setMessage(pnsMessageDetail.getMessage());
        pushNotificationEntity.setStatus(pnsMessageDetail.getStatus().shortValue());
        pushNotificationEntity.setPnsType(pnsTypeId);
        pnsdao.insertOrUpdatePNS(pushNotificationEntity);
    }

    private void populatePNSNotificationV2(PNSMessageDBDetail pnsMessageDBDetail) {
        LOG.info("test-Inside Notification - sendMessage:populatePNSNotificationV2*******");
        PushNotificationEntityV2 pushNotificationEntity = new PushNotificationEntityV2();
        pushNotificationEntity.setNotificationRequestId(pnsMessageDBDetail.getNotificationRequestId());
        pushNotificationEntity.setMessage(pnsMessageDBDetail.getMessage());
        pushNotificationEntity.setDriverCustomerId(pnsMessageDBDetail.getDriverCustomerId());
        pushNotificationEntity.setMessageToken(pnsMessageDBDetail.getMessageToken());
        pushNotificationEntity.setPnsType(pnsMessageDBDetail.getPnsType());
        pnsdao.insertOrUpdatePNS_V2(pushNotificationEntity);
    }

    /**
     *
     */
    private void validateEmail(EmailMessage emailMessage, String templateName) throws NotificationException {

        if ((emailMessage.getToRecipients() == null || emailMessage.getToRecipients().isEmpty())
                && (emailMessage.getBccRecipients() == null || emailMessage.getBccRecipients().isEmpty())) {
            throw new NotificationException("Email recepients cannot be empty");
        }

        if (templateName == null || templateName.isEmpty()) {
            throw new NotificationException("Email template not found");
        }
    }

    /**
     *
     */
    private void validateSMS(SMSMessage smsMessage, String templateName) throws NotificationException {

        if (smsMessage.getToRecipients() == null || smsMessage.getToRecipients().isEmpty()) {
            throw new NotificationException("SMS recepients cannot be empty");
        }

        if (templateName == null || templateName.isEmpty()) {
            throw new NotificationException("SMS template not found");
        }
    }

    /**
     *
     */
    private void validatePNS(PNSMessage pnsMessage, String templateName) throws NotificationException {
        Gson gson = new Gson();
        LOG.info("test-validatePNS--------------------->" + gson.toJson(pnsMessage.getDeviceInfoDTOs()));
        if (pnsMessage.getDeviceInfoDTOs() == null || pnsMessage.getDeviceInfoDTOs().isEmpty()) {
            throw new NotificationException("Device registration token cannot be empty");
        }

        if (templateName == null || templateName.isEmpty()) {
            throw new NotificationException("PNS template not found");
        }
    }
}
