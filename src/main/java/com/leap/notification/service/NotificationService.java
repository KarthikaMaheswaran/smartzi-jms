/**
 * Copyright (C) MiA - Chalico Ltd.
 * Confidential and proprietary.
 */
package com.leap.notification.service;

import com.leap.exception.NotificationException;
import com.leap.notification.model.NotificationRequest;

/**
 * Common Services to send notification
 * 
 */
public interface NotificationService {

    /**
     * 
     * To initiate send notification
     * 
     * @param messageRequest messageRequest
     * @param notificationRequestId notificationRequestId
     * @throws NotificationException NotificationException
     */
    void sendMessage(NotificationRequest messageRequest, Long notificationRequestId) throws NotificationException;

}
