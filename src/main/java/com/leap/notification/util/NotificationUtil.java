package com.leap.notification.util;

import com.leap.jms.dto.DeviceInfoDTO;
import com.leap.notification.model.*;
import org.springframework.ui.ModelMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class NotificationUtil {

	/**
	 * 
	 * @param emailMessage
	 * @param smsMessage
	 * @param pnsMessage
	 * @param templateID
	 * @return
	 */
	public static NotificationRequest getNotificationRequest(EmailMessage emailMessage, SMSMessage smsMessage,
                                                             PNSMessage pnsMessage, String templateID) {
		NotificationRequest notificationRequest = new NotificationRequest();
		notificationRequest.setEmailMessage(emailMessage);
		notificationRequest.setSmsMessage(smsMessage);
		notificationRequest.setPnsMessage(pnsMessage);
		notificationRequest.setTemplateID(templateID);
		return notificationRequest;
	}

	/**
	 * 
	 * @param toRecipients
	 * @param placeHolderData
	 * @return
	 */
	public static SMSMessage getSMSMessage(List<String> toRecipients, Map<String, Object> placeHolderData) {
		SMSMessage smsMessage = new SMSMessage();
		smsMessage.setToRecipients(toRecipients);
		smsMessage.setPlaceHolderData(placeHolderData);
		return smsMessage;
	}

	/**
	 * 
	 * @param subject
	 * @param toRecipients
	 * @param ccRecipients
	 * @param placeHolderData
	 * @param attachments
	 * @return
	 */
	public static EmailMessage getEmailMessage(String subject, List<String> toRecipients, List<String> ccRecipients,
			List<String> bccRecipients, Map<String, Object> placeHolderData, List<Attachment> attachments) {
		EmailMessage emailMessage = new EmailMessage();
		emailMessage.setSubject(subject);
		emailMessage.setToRecipients(toRecipients);
		emailMessage.setCcRecipients(ccRecipients);
		emailMessage.setBccRecipients(bccRecipients);
		emailMessage.setPlaceHolderData(placeHolderData);
		emailMessage.setAttachments(attachments);
		return emailMessage;
	}

	/**
	 * 
	 * @param deviceInfoDTOs
	 * @param placeHolderData
	 * @param payloadParams
	 * @return
	 */
	public static PNSMessage getPNSMessage(List<DeviceInfoDTO> deviceInfoDTOs, Map<String, Object> placeHolderData,
                                           Map<String, Object> payloadParams) {
		PNSMessage pnsMessage = new PNSMessage();
		pnsMessage.setDeviceInfoDTOs(deviceInfoDTOs);
		pnsMessage.setPlaceHolderData(placeHolderData);
		pnsMessage.setPayloadParams(payloadParams);
		return pnsMessage;
	}

	/**
	 * 
	 * @param subject
	 * @param toRecipients
	 * @param ccRecipients
	 * @param attachments
	 * @return
	 */
	public static EmailMessage getFutureTripEmailMessage(String subject, List<String> toRecipients,
			List<String> ccRecipients, List<String> bccRecipients, ModelMap map, List<Attachment> attachments) {
		EmailMessage emailMessage = new EmailMessage();
		emailMessage.setSubject(subject);
		emailMessage.setToRecipients(toRecipients);
		emailMessage.setCcRecipients(ccRecipients);
		emailMessage.setBccRecipients(bccRecipients);
		emailMessage.setAttachments(attachments);
		return emailMessage;
	}

}
