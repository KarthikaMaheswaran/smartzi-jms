package com.leap.notification.model;

/**
 * @author Hisoban
 * @date 16-Jul-20
 */
public class SMSMessageDBDetail {

    private Long notificationRequestId;
    private String message;
    private String mobileNo[];
    private String messageToken[];

    public Long getNotificationRequestId() {
        return notificationRequestId;
    }

    public void setNotificationRequestId(Long notificationRequestId) {
        this.notificationRequestId = notificationRequestId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String[] getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String[] mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String[] getMessageToken() {
        return messageToken;
    }

    public void setMessageToken(String[] messageToken) {
        this.messageToken = messageToken;
    }
}
