/**
 * Copyright (C) MiA - Chalico Ltd.
 * Confidential and proprietary.
 */
package com.leap.notification.model;

/**
 * 
 * This class contain notification request details
 * 
 */
public class NotificationRequest {
    /**
     * 
     */
    private EmailMessage emailMessage;
    /**
     * 
     */
    private SMSMessage smsMessage;
    /**
     * 
     */
    private PNSMessage pnsMessage;
    /**
     * 
     */
    private String templateID;

    /**
     * 
     */
    public EmailMessage getEmailMessage() {
        return emailMessage;
    }

    /**
     * 
     */
    public void setEmailMessage(EmailMessage emailMessage) {
        this.emailMessage = emailMessage;
    }

    /**
     * 
     */
    public SMSMessage getSmsMessage() {
        return smsMessage;
    }

    /**
     * 
     */
    public void setSmsMessage(SMSMessage smsMessage) {
        this.smsMessage = smsMessage;
    }

    /**
     * 
     */
    public PNSMessage getPnsMessage() {
        return pnsMessage;
    }

    /**
     * 
     */
    public void setPnsMessage(PNSMessage pnsMessage) {
        this.pnsMessage = pnsMessage;
    }

    /**
     * 
     */
    public String getTemplateID() {
        return templateID;
    }

    /**
     * 
     */
    public void setTemplateID(String templateID) {
        this.templateID = templateID;
    }

}
