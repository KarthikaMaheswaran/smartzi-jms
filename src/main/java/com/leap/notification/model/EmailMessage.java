package com.leap.notification.model;

import java.util.List;
import java.util.Map;

/**
 * This class models an email message. Message contains content, sender
 * informations and receiver information.
 * 
 */
public class EmailMessage {
    /**
     * 
     */
    private String subject;
    /**
     * 
     */
    private List<String> toRecipients;
    /**
     * 
     */
    private List<String> ccRecipients;
    /**
     * 
     */
    private List<String> bccRecipients;
    /**
     * 
     */
    private Map<String, Object> placeHolderData;
    /**
     * 
     */
    private List<Attachment> attachments;

    /**
     *
     */
    private boolean isAttachment;

    /**
     *
     */
    private String attchmentUrl;

    /**
     *
     */
    private String bucketName;

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public boolean isAttachment() {
        return isAttachment;
    }

    public void setAttachment(boolean attachment) {
        isAttachment = attachment;
    }

    public String getAttchmentUrl() {
        return attchmentUrl;
    }

    public void setAttchmentUrl(String attchmentUrl) {
        this.attchmentUrl = attchmentUrl;
    }


    public String getSubject() {
        return subject;
    }

    /**
     * 
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * 
     */
    public List<String> getToRecipients() {
        return toRecipients;
    }

    /**
     * 
     */
    public void setToRecipients(List<String> toRecipients) {
        this.toRecipients = toRecipients;
    }

    /**
     * 
     */
    public List<String> getCcRecipients() {
        return ccRecipients;
    }

    /**
     * 
     */
    public void setCcRecipients(List<String> ccRecipients) {
        this.ccRecipients = ccRecipients;
    }

    /**
     * 
     */
    public Map<String, Object> getPlaceHolderData() {
        return placeHolderData;
    }

    /**
     * 
     */
    public void setPlaceHolderData(Map<String, Object> placeHolderData) {
        this.placeHolderData = placeHolderData;
    }

    /**
     * 
     */
    public List<Attachment> getAttachments() {
        return attachments;
    }

    /**
     * 
     */
    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    /**
     * @return the bccRecipients
     */
    public List<String> getBccRecipients() {
        return bccRecipients;
    }

    /**
     * @param bccRecipients
     *            the bccRecipients to set
     */
    public void setBccRecipients(List<String> bccRecipients) {
        this.bccRecipients = bccRecipients;
    }



}
