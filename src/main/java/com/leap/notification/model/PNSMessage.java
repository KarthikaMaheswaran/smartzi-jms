package com.leap.notification.model;



import com.leap.jms.dto.DeviceInfoDTO;

import java.util.List;
import java.util.Map;

/**
 * 
 * This class contain pns message details
 * 
 */
public class PNSMessage {
    /**
     * 
     */
    private List<DeviceInfoDTO> deviceInfoDTOs;
    /**
     * 
     */
    private Map<String, Object> placeHolderData;
    /**
     * 
     */
    private Map<String, Object> payloadParams;

    /**
     * 
     */
    public Map<String, Object> getPlaceHolderData() {
        return placeHolderData;
    }

    /**
     * 
     */
    public void setPlaceHolderData(Map<String, Object> placeHolderData) {
        this.placeHolderData = placeHolderData;
    }

    /**
     * 
     */
    public Map<String, Object> getPayloadParams() {
        return payloadParams;
    }

    /**
     * 
     */
    public void setPayloadParams(Map<String, Object> payloadParams) {
        this.payloadParams = payloadParams;
    }

    /**
     * @return the deviceInfoDTOs
     */
    public List<DeviceInfoDTO> getDeviceInfoDTOs() {
        return deviceInfoDTOs;
    }

    /**
     * @param deviceInfoDTOs
     *            the deviceInfoDTOs to set
     */
    public void setDeviceInfoDTOs(List<DeviceInfoDTO> deviceInfoDTOs) {
        this.deviceInfoDTOs = deviceInfoDTOs;
    }

}
