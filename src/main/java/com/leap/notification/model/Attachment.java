package com.leap.notification.model;

/**
 * 
 * This class contains attachment details
 * 
 */
public class Attachment {
    /**
     * 
     */
    private byte[] attchmentContent;
    /**
     * 
     */
    private String fileName;
    /**
     * 
     */
    private String contentType;

    /**
     * 
     */
    public byte[] getAttchmentContent() {
        return attchmentContent;
    }

    /**
     * 
     */
    public void setAttchmentContent(byte[] attchmentContent) {
        this.attchmentContent = attchmentContent;
    }

    /**
     * 
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * 
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * 
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * 
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

}
