package com.leap.notification.model;

/**
 * @author Hisoban
 * @date 16-Jul-20
 */
public class PNSMessageDBDetail {

    private Long notificationRequestId;
    private String message;
    private String driverCustomerId[];
    private String messageToken[];
    private Integer pnsType[];

    public Long getNotificationRequestId() {
        return notificationRequestId;
    }

    public void setNotificationRequestId(Long notificationRequestId) {
        this.notificationRequestId = notificationRequestId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String[] getDriverCustomerId() {
        return driverCustomerId;
    }

    public void setDriverCustomerId(String[] driverCustomerId) {
        this.driverCustomerId = driverCustomerId;
    }

    public String[] getMessageToken() {
        return messageToken;
    }

    public void setMessageToken(String[] messageToken) {
        this.messageToken = messageToken;
    }

    public Integer[] getPnsType() {
        return pnsType;
    }

    public void setPnsType(Integer[] pnsType) {
        this.pnsType = pnsType;
    }
}
