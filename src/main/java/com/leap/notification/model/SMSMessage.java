/**
 * Copyright (C) MiA - Chalico Ltd.
 * Confidential and proprietary.
 */
package com.leap.notification.model;

import java.util.List;
import java.util.Map;

/**
 * 
 * This class contain sms message details
 * 
 */
public class SMSMessage {

    private List<String> toRecipients;
    private Map<String, Object> placeHolderData;

    /**
     * @return the toRecipients
     */
    public List<String> getToRecipients() {
        return toRecipients;
    }

    /**
     * @param toRecipients
     *            the toRecipients to set
     */
    public void setToRecipients(List<String> toRecipients) {
        this.toRecipients = toRecipients;
    }

    /**
     * @return the placeHolderData
     */
    public Map<String, Object> getPlaceHolderData() {
        return placeHolderData;
    }

    /**
     * @param placeHolderData
     *            the placeHolderData to set
     */
    public void setPlaceHolderData(Map<String, Object> placeHolderData) {
        this.placeHolderData = placeHolderData;
    }

}
