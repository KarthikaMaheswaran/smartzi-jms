package com.leap.notification.pns.constant;

/**
 * 
 * All pns related constants to go here
 * 
 */
public class PNSConstant {
    /**
     * 
     */
    public interface PnsStatus {
        /**
         * 
         */
        String NOT_REGISTERED = "NotRegistered";
    }

    /**
     * 
     */
    public interface PayLoadField {
        /**
         * 
         */
        String MESSAGE = "message";
        /**
         * 
         */
        String MESSAGE_TOKEN = "messageToken";
        /**
         * 
         */
        String SENT_DATE_TIME = "sentDateTime";
        /**
         * 
         */
        String TRIP_ID = "tripId";
    }

    /**
     * 
     */
    public interface PnsParam {
        /**
         * 
         */
        String HEADER_ENTRIES = "headerEntries";
        /**
         * 
         */
        String APP_CERTIFICATE = "appCertificate";
        /**
         * 
         */
        String CERT_PATH = "certPath";
        /**
         * 
         */
        String CERT_PD = "certPwd";
        /**
         * 
         */
        String SID = "sid";
        /**
         * 
         */
        String CLIENT_SECRET = "clientSecret";
        /**
         * 
         */
        String USE_TEST = "useTest";
    }

    /**
     * 
     */
    public interface NotificationStatus {
        /**
         * 
         */
        Integer FAILED = 4;
        /**
         * 
         */
        Integer SENT = 1;
        /**
         * 
         */
        Integer UNSENT = 0;
        /**
         * 
         */
        Integer RETRYING = 5;
        /**
         * 
         */
        Integer ACKNOWLEDGED = 2;
        /**
         * 
         */
        Integer INACTIVE_DEVICE_TOKEN = 6;
    }

    /**
     * 
     */
    public interface PnsAttribute {
        /**
         * 
         */
        String CUSTOM_DATAS = "customDatas";

        /**
         * 
         */
        public interface GCM {
            /**
             * 
             */
            String TIME_TO_LIVE = "time_to_live";
            /**
             * 
             */
            String CUSTOM_DATAS = "customDatas";
            /**
             * 
             */
            String DELAY_WHILE_IDLE = "delay_while_idle";
            /**
             * 
             */
            String COLLAPSE_KEY = "collapse_key";
            /**
             * 
             */
            String RESTRICTED_PACKAGE_NAME = "restricted_package_name";

            ///FCM END POINT

            String FCM_END_POINT="https://fcm.googleapis.com/fcm/send";
        }

        /**
         * 
         */
        public interface Apns {
            /**
             * 
             */
            String ALERT_LOC_KEY = "loc-key";
            /**
             * 
             */
            String ALERT_LOC_ARGS = "loc-args";
            /**
             * 
             */
            String LAUNCH_IMAGE = "launch-image";
            /**
             * 
             */
            String BADGE = "badge";
            /**
             * 
             */
            String SOUND = "notify.aiff";
            /**
             * 
             */
            String CATEGORY = "category";
            /**
             * 
             */
            String ALERT_TITLE = "title";
            /**
             * 
             */
            String TITLE_LOC_KEY = "title-loc-key";
            /**
             * 
             */
            String TITLE_LOC_ARGS = "title-loc-args";
            /**
             * 
             */
            String ALERT_ACTION = "action";
            /**
             * 
             */
            String ACTION_LOC_KEY = "action-loc-key";
            /**
             * 
             */
            String SILENT_NOTIFICATION = "content-available";
            /**
             * 
             */
            String MDM = "mdm";
            /**
             * 
             */
            String CUSTOM_FIELDS = "customFields";
        }
    }
}
