package com.leap.notification.pns;


import com.leap.exception.PNSException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.leap.notification.pns.factory.PushNotificationFactory;
import com.leap.notification.pns.model.PushNotification;
import com.leap.notification.pns.service.PushNotificationService;

/**
 * 
 * This class is used to send push notification
 * 
 */
public class PushNotificationSender {

    private static final Logger LOG = Logger.getLogger(PushNotificationSender.class);

    /**
     * 
     */
    @Autowired
    private PushNotificationService pushNotificationService;

    /**
     * Constructor that takes PNS Type .
     * 
     * @param platformType
     *            Supported types are : 1(GCM), 2(APNS)
     */
    public PushNotificationSender(Short platformType) {
        this.pushNotificationService = PushNotificationFactory.createService(platformType);
    }

    /**
     * This method pushes the notification to the specified PNS.
     * 
     * @return this method will return true if success otherwise throws
     *         exception.
     * @throws PNSException PNSException
     */
    public boolean sendMessage(PushNotification pushNotification) throws PNSException {
        pushNotificationService.push(pushNotification);
        return true;
    }
}
