/**
 * Copyright (C) MiA - Chalico Ltd.
 * Confidential and proprietary.
 */
package com.leap.notification.pns.service.impl;

import java.util.*;


import com.google.gson.Gson;
import com.leap.dao.GCMServiceDAO;
import com.leap.jms.dto.request.DeleteInvalidDeviceTokenRequest;
import com.leap.notification.pns.constant.PNSConstant;
import com.leap.notification.pns.model.PNSMessageDetail;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import com.notnoop.apns.ApnsService;
import com.notnoop.apns.PayloadBuilder;
import com.notnoop.apns.APNS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.leap.notification.pns.model.PushNotification;


/**
 * This class contains properties related to Apns
 */
@Component
public class APNSService {
//public class APNSService implements PushNotificationService {

    private static final Logger LOG = Logger.getLogger(APNSService.class);

    private ApnsService service;

    @Autowired
    private GCMServiceDAO gcmServiceDAO;

    /**
     *
     */
    private void initialiseService(Map<String, Object> initParams) {
        if (initParams.get(PNSConstant.PnsParam.USE_TEST) != null
                && (Boolean) initParams.get(PNSConstant.PnsParam.USE_TEST)) {
            service = APNS
                    .newService()
                    .withCert((String) initParams.get(PNSConstant.PnsParam.CERT_PATH),
                            (String) initParams.get(PNSConstant.PnsParam.CERT_PD)).withAppleDestination(false).build();
        } else {
            service = APNS
                    .newService()
                    .withCert((String) initParams.get(PNSConstant.PnsParam.CERT_PATH),
                            (String) initParams.get(PNSConstant.PnsParam.CERT_PD)).withAppleDestination(true).build();
        }
        service.start();
    }

    /**
     *
     */
    @SuppressWarnings("unchecked")
    private String formPayload(PushNotification pushNotification) {
        PayloadBuilder payloadBuilder = APNS.newPayload();
        payloadBuilder.alertBody(pushNotification.getPayLoad());
        String category = (String) pushNotification.getPayloadParams().get(PNSConstant.PnsAttribute.Apns.CATEGORY);
        String alertLocKey = (String) pushNotification.getPayloadParams().get(
                PNSConstant.PnsAttribute.Apns.ALERT_LOC_KEY);
        List<String> alertLocArgs = (List<String>) pushNotification.getPayloadParams().get(
                PNSConstant.PnsAttribute.Apns.ALERT_LOC_ARGS);
        String launchImage = (String) pushNotification.getPayloadParams()
                .get(PNSConstant.PnsAttribute.Apns.LAUNCH_IMAGE);
        Integer badge = (Integer) pushNotification.getPayloadParams().get(PNSConstant.PnsAttribute.Apns.BADGE);
        String sound = PNSConstant.PnsAttribute.Apns.SOUND;//(String) pushNotification.getPayloadParams().get(PNSConstant.PnsAttribute.Apns.SOUND);
        String title = (String) pushNotification.getPayloadParams().get(PNSConstant.PnsAttribute.Apns.ALERT_TITLE);
        String titleLocKey = (String) pushNotification.getPayloadParams().get(
                PNSConstant.PnsAttribute.Apns.TITLE_LOC_KEY);
        List<String> titleLocArgs = (List<String>) pushNotification.getPayloadParams().get(
                PNSConstant.PnsAttribute.Apns.TITLE_LOC_ARGS);
        String alertAction = (String) pushNotification.getPayloadParams()
                .get(PNSConstant.PnsAttribute.Apns.ALERT_ACTION);
        String actionLocKey = (String) pushNotification.getPayloadParams().get(
                PNSConstant.PnsAttribute.Apns.ACTION_LOC_KEY);
        String mdm = (String) pushNotification.getPayloadParams().get(PNSConstant.PnsAttribute.Apns.MDM);
        Map<String, Object> customFields = (Map<String, Object>) pushNotification.getPayloadParams().get(
                PNSConstant.PnsAttribute.CUSTOM_DATAS);
        if (customFields == null) {
            customFields = new HashMap<>();
        }
        customFields.put(PNSConstant.PayLoadField.MESSAGE_TOKEN, pushNotification.getMessageDetails().get(0)
                .getMessageToken());
        setDate(customFields); // TODO Remove
        if (category != null) {
            payloadBuilder.category(category);
        }
        if (alertLocKey != null) {
            payloadBuilder.localizedKey(alertLocKey);
        }
        if (alertLocArgs != null) {
            payloadBuilder.localizedArguments(alertLocArgs);
        }
        if (launchImage != null) {
            payloadBuilder.launchImage(launchImage);
        }
        if (badge != null) {
            payloadBuilder.badge(badge);
        }
        if (sound != null) {
            payloadBuilder.sound(sound);
        }
        if (title != null) {
            payloadBuilder.alertTitle(title);
        }
        if (titleLocKey != null) {
            payloadBuilder.localizedTitleKey(titleLocKey);
        }
        if (titleLocArgs != null && titleLocArgs.size() > 0) {
            payloadBuilder.localizedTitleArguments(titleLocArgs);
        }
        if (alertAction != null) {
            payloadBuilder.alertAction(alertAction);
        }
        if (actionLocKey != null) {
            payloadBuilder.actionKey(actionLocKey);
        }
        if (mdm != null) {
            payloadBuilder.mdm(mdm);
        }
        if (customFields != null) {
            payloadBuilder.customFields(customFields);
        }
        if (pushNotification.getPayloadParams().get(PNSConstant.PnsAttribute.Apns.SILENT_NOTIFICATION) != null) {
            payloadBuilder.instantDeliveryOrSilentNotification();
        }
        return payloadBuilder.build();
    }

    /**
     *
     */
    // TODO Remove this after demo
    private void setDate(Map<String, Object> customFields) {
        DateTime utc = new DateTime(DateTimeZone.UTC);
        String time = utc.toString().replace("T", " ").replace("Z", " ");
        customFields.put(PNSConstant.PayLoadField.SENT_DATE_TIME, time.substring(0, time.lastIndexOf(".")));
    }

    /**
     *
     */
    /*public void push(PushNotification pushNotification) {

        String payLoad = formPayload(pushNotification);
        for (PNSMessageDetail messageDetail : pushNotification.getMessageDetails()) {
            initialiseService(messageDetail.getInitParams());
            messageDetail.setMessage(pushNotification.getPayLoad());
            try {
                service.push(messageDetail.getDeviceRegistrationToken(), payLoad);
                messageDetail.setStatus(PNSConstant.NotificationStatus.SENT);
            } catch (Exception e) {
                messageDetail.setStatus(PNSConstant.NotificationStatus.FAILED);
                if (LOG.isInfoEnabled()) {
                    LOG.info("Failed to send ios push notification to : " + messageDetail.getDeviceRegistrationToken()
                            + ", Cause : " + e.toString());
                }
            }
        }
        if (service != null) {
            service.stop();
        }
    }*/
    public void push(PushNotification pushNotification) {
        Gson gson = new Gson();
        LOG.info("pushNotification---------------------"+gson.toJson(pushNotification));
        String payLoad = formPayload(pushNotification);
        LOG.info("pushNotification---------------------"+gson.toJson(payLoad));

        Collection<String> messageDetailLst = new ArrayList<>();
        String response = null;
        Map<String, Date> inactiveDevices = new HashMap<String, Date>();
        try {
            for (PNSMessageDetail messageDetail : pushNotification.getMessageDetails()) {
                initialiseService(messageDetail.getInitParams());
                messageDetail.setMessage(pushNotification.getPayLoad());
                messageDetailLst.add(messageDetail.getDeviceRegistrationToken());
            }

            response = String.valueOf(service.push(messageDetailLst, payLoad));
            LOG.info("response------------------"+response);
//            LOG.info("Inside Apns Servive - response:" + response);
            inactiveDevices = service.getInactiveDevices();
            LOG.info("Inside Apns Servive - inactiveDevices:" + inactiveDevices);

            if (inactiveDevices != null && inactiveDevices.size() > 0) {
                for (Map.Entry<String, Date> entry : inactiveDevices.entrySet()) {
                    String deviceDoken = entry.getKey();
                    LOG.info("Inside Apns Servive - push inactiveDevice:" + deviceDoken);
                    DeleteInvalidDeviceTokenRequest deleteInvalidDeviceTokenReq = new DeleteInvalidDeviceTokenRequest();
                    deleteInvalidDeviceTokenReq.setDeviceRegistrationToken(deviceDoken);
                    deleteInvalidDeviceTokenReq.setAppPlatformId((short) 2);
                    gcmServiceDAO.deleteInvalidDeviceToken(deleteInvalidDeviceTokenReq);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (LOG.isInfoEnabled()) {
                LOG.info("Failed to send ios push notification, Cause : " + e.toString());
            }
        }
        if (service != null) {
            service.stop();
        }
    }

    public void GCMPush(PushNotification pushNotification) {

        String payLoad = formPayload(pushNotification);
        Collection<String> messageDetailLst = new ArrayList<>();
        try {
            for (PNSMessageDetail messageDetail : pushNotification.getMessageDetails()) {
                initialiseService(messageDetail.getInitParams());
                messageDetail.setMessage(pushNotification.getPayLoad());
                messageDetailLst.add(messageDetail.getDeviceRegistrationToken());
            }
            service.push(messageDetailLst, payLoad);
        } catch (Exception e) {
            if (LOG.isInfoEnabled()) {
                LOG.info("Failed to send ios push notification, Cause : " + e.toString());
            }
        }
        if (service != null) {
            service.stop();
        }
    }

    /**
     *
     */
    /*public static void main(String[] args) throws PNSException {
        PushNotificationService ApnsService = new APNSService();
        PushNotification pushNotification = new PushNotification();
        List<PNSMessageDetail> pnsMessageDetails = new ArrayList<>();
        PNSMessageDetail pnsMessageDetail = new PNSMessageDetail();
        pnsMessageDetail.getInitParams().put(PNSConstant.PnsParam.USE_TEST, Boolean.FALSE);
        pnsMessageDetail.getInitParams().put(PNSConstant.PnsParam.CERT_PATH,
                "D:/office/P12Certificate-IOS/Driver_APNS_Certificates.p12");
        pnsMessageDetail.getInitParams().put(PNSConstant.PnsParam.CERT_PD, "Smart@2020$");
        pnsMessageDetail.setDeviceRegistrationToken("1a8ec82f4f55ac41b065f64b99158b63a7b50001159bd69630b403fa631e9800");//1a8ec82f4f55ac41b065f64b99158b63a7b50001159bd69630b403fa631e9741
        pnsMessageDetails.add(pnsMessageDetail);
        pushNotification.setMessageDetails(pnsMessageDetails);
        pushNotification.setPayLoad("This is test notification");
        ApnsService.push(pushNotification);
    }*/
}
