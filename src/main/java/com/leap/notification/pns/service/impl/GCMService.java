/**
 * Copyright (C) MiA - Chalico Ltd.
 * Confidential and proprietary.
 */
package com.leap.notification.pns.service.impl;

import java.io.*;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.leap.dao.GCMServiceDAO;
import com.leap.jms.dto.request.DeleteInvalidDeviceTokenRequest;
import com.leap.notification.pns.constant.PNSConstant;
import com.leap.notification.pns.model.PNSMessageDetail;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Message.Builder;
import com.google.android.gcm.server.Sender;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import com.leap.notification.pns.model.PushNotification;

/**
 * 
 * This class contains properties related to GCM
 * 
 */
@Component
public class GCMService {
//public class GCMService implements PushNotificationService {

    private static final Logger LOG = Logger.getLogger(GCMService.class);

    @Autowired
    private GCMServiceDAO gcmServiceDAO;

    private Sender sender;

    protected final java.util.logging.Logger logger = java.util.logging.Logger.getLogger(getClass().getName());

    private  String messageId;


    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getCanonicalRegistrationId() {
        return canonicalRegistrationId;
    }

    public void setCanonicalRegistrationId(String canonicalRegistrationId) {
        this.canonicalRegistrationId = canonicalRegistrationId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    private  String canonicalRegistrationId;

    private  String errorCode;

    public String getErrorCodeName() {
        return errorCode;
    }



    private void initialiseService(Map<String, Object> initParams) {
        sender = new Sender((String) initParams.get(PNSConstant.PnsParam.APP_CERTIFICATE));
    }

    /**
     * 
     */
    @SuppressWarnings("unchecked")
    private Message formPayload(PushNotification pushNotification) throws UnsupportedEncodingException {
        Builder builder = new Message.Builder();

        Map<String, String> customDatas = (Map<String, String>) pushNotification.getPayloadParams().get(
                PNSConstant.PnsAttribute.CUSTOM_DATAS);
        if (customDatas == null) {
            customDatas = new HashMap<>();
        }
        customDatas.put(PNSConstant.PayLoadField.MESSAGE, URLEncoder.encode(pushNotification.getPayLoad(), "utf-8"));
        customDatas.put(PNSConstant.PayLoadField.MESSAGE_TOKEN, pushNotification.getMessageDetails().get(0)
                .getMessageToken());
        setDate(customDatas); // TODO Remove
        Integer timeToLive = (Integer) pushNotification.getPayloadParams()
            .get(PNSConstant.PnsAttribute.GCM.TIME_TO_LIVE);
        if (timeToLive != null) {
            builder.timeToLive(timeToLive);
        }
        builder.setData(customDatas);
        return builder.build();
    }

    /**
     * 
     */
    // TODO Remove this after demo
    private void setDate(Map<String, String> customDatas) {
        DateTime utc = new DateTime(DateTimeZone.UTC);
        String time = utc.toString().replace("T", " ").replace("Z", " ");
        customDatas.put(PNSConstant.PayLoadField.SENT_DATE_TIME, time.substring(0, time.lastIndexOf(".")));
    }

    /**
     * To send push notification
     */
    public void push(PushNotification pushNotification) {
        for (PNSMessageDetail messageDetail : pushNotification.getMessageDetails()) {

            initialiseService(messageDetail.getInitParams());
            messageDetail.setMessage(pushNotification.getPayLoad());
            try {

                // GCMService result = fcmPush(messageDetail);
                String response=sendFCM(messageDetail);
                LOG.info("response FCM******************"+response);
                LOG.info("pushnotification******************"+pushNotification.getPayLoad());
                LOG.info("messageDetail-getDeviceRegistrationToken**** " + messageDetail.getDeviceRegistrationToken());
                LOG.info("messageDetail-getPlatformType**** " + messageDetail.getPlatformType());

                // adden on 04-Aug-2020
                if(response != null) {
                    ObjectMapper objectMapper = new ObjectMapper();
                    JsonNode jsonNode = objectMapper.readTree(response);
                    JsonNode f2FieldNode = jsonNode.get("results").get(0).get("error");
                    if(f2FieldNode != null) {
                        LOG.info("Inside messageDetail-getPlatformType: f2FieldNode****");
                        DeleteInvalidDeviceTokenRequest deleteInvalidDeviceTokenReq = new DeleteInvalidDeviceTokenRequest();
                        deleteInvalidDeviceTokenReq.setDeviceRegistrationToken(messageDetail.getDeviceRegistrationToken());
                        deleteInvalidDeviceTokenReq.setAppPlatformId(messageDetail.getPlatformType());
                        gcmServiceDAO.deleteInvalidDeviceToken(deleteInvalidDeviceTokenReq);
                    }
                }
                LOG.info("Inside messageDetail- out side condition ");
                messageDetail.setStatus(PNSConstant.NotificationStatus.SENT);


                // GCM will return null in error response code name when the
                // notification is sent successfully
                /*if (result.getErrorCodeName() != null) {
                    // checking for inactive device tokens
                    if (result.getErrorCodeName().equalsIgnoreCase(PNSConstant.PnsStatus.NOT_REGISTERED)) {
                        messageDetail.setStatus(PNSConstant.NotificationStatus.INACTIVE_DEVICE_TOKEN);
                    }
                }*/
            } catch (Exception e) {
                messageDetail.setStatus(PNSConstant.NotificationStatus.FAILED);
                if (LOG.isInfoEnabled()) {
                    LOG.info("Failed to send android push notification to : "
                            + messageDetail.getDeviceRegistrationToken() + ", Cause : " + e.toString());
                }
            }
        }
    }

public String sendFCM(PNSMessageDetail pnsMessageDetail)
{

    String androidFcmKey="";
    Iterator<Map.Entry<String, Object>> fcmAPIKey = pnsMessageDetail.getInitParams().entrySet().iterator();
    while (fcmAPIKey.hasNext())
    {
        Map.Entry<String,Object> fcmValue=fcmAPIKey.next();
        if(PNSConstant.PnsParam.APP_CERTIFICATE.equals(fcmValue.getKey()))
            androidFcmKey= fcmValue.getValue().toString();
        logger.info("APP_CERTIFICATE FCM *********"+fcmValue.getValue());

    }
    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders httpHeaders = new HttpHeaders();

    httpHeaders.set("Authorization", "key="+androidFcmKey);
    httpHeaders.set("Content-Type", "application/json");
    JSONObject msg = new JSONObject();
    JSONObject json = new JSONObject();
    msg.put("message", pnsMessageDetail.getMessage());
    json.put("data", msg);
    json.put("to", pnsMessageDetail.getDeviceRegistrationToken());
    HttpEntity<String> httpEntity = new HttpEntity<String>(json.toString(), httpHeaders);
    String response = restTemplate.postForObject(PNSConstant.PnsAttribute.GCM.FCM_END_POINT, httpEntity, String.class);
//    System.out.println("FCM Response ****************#############"+response);
    return response;
}
    /*public static void main(String[] args) {
        Message message = new Message.Builder().addData("message", "this is the message")
                .addData("other-parameter", "some value").build();
        try {
            Sender sender = new Sender("AIzaSyAvpW9jOAZb2GAIpgZ_d8__S0Ylmn7MwUk");
            Result result = sender
                .sendNoRetry(
                    message,
                    "APA91bH5y2wT8_ZRbL2gRYBtiG7qu0sABP3D_54AMPN9giPkDyLnUL8hsMGntWdE2TXDNxHFXRrmAj"
                        + "VdJzNzRWA6VJNayEp99SJVPaex0p0GNkU8VlhIg2-6-HyT5lA2kFvTZUxcHNgz");
            if (LOG.isInfoEnabled()) {
                LOG.info("The repsonse getMessageId :: " + result.getMessageId());
                LOG.info("The repsonse getErrorCodeName :: " + result.getErrorCodeName());
                LOG.info("The repsonse getCanonicalRegistrationId :: " + result.getCanonicalRegistrationId());
            }
        } catch (IOException e) {
            if (LOG.isInfoEnabled()) {
                LOG.info("Failed to send android push notification, Cause : " + e.toString());
            }
        }
    }*/

}
