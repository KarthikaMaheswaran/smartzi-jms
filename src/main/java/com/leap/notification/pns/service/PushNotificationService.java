/**
 * Copyright (C) MiA - Chalico Ltd.
 * Confidential and proprietary.
 */
package com.leap.notification.pns.service;


import com.leap.exception.PNSException;
import com.leap.notification.pns.model.PushNotification;

/**
 * This class is used to send push notification
 */
public interface PushNotificationService {
    /**
     *
     */
    void push(PushNotification pushNotificationDTO) throws PNSException;
}
