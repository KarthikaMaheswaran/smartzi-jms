package com.leap.notification.pns.factory;


import com.leap.common.constant.AppPlatformEnum;
import com.leap.notification.pns.service.PushNotificationService;

/**
 * 
 * This class is used to decide the type of pns while sending push notification
 * 
 */
public class PushNotificationFactory {
    /**
     * 
     */
    private PushNotificationFactory() {
        // private constructor
    }

    /**
     * 
     */
    public static PushNotificationService createService(Short platformType) {
        PushNotificationService pushNotificationService = null;
        if (platformType.equals(AppPlatformEnum.ios.getId())) {
//            pushNotificationService = new APNSService();
        } else {
//            pushNotificationService = new GCMService();
        }
        return pushNotificationService;
    }
}
