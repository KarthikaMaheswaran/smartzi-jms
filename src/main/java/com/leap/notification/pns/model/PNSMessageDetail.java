package com.leap.notification.pns.model;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * Model to hold PNS specific device registration Token and status
 * 
 */
public class PNSMessageDetail {

    private String message;
    private String messageToken;
    private String deviceRegistrationToken;
    private Long deviceId;
    private Short platformType;
    private String pnsType;

    public String getPnsType() {
        return pnsType;
    }

    public void setPnsType(String pnsType) {
        this.pnsType = pnsType;
    }

    /**
     * 
     */
    private Map<String, Object> initParams = new HashMap<>();

    /**
     * Status of message. possible value 0 - Failed, 1 - Sent
     */
    private Integer status = 0;

    /**
     * 
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     */
    public String getMessageToken() {
        return messageToken;
    }

    /**
     * 
     */
    public void setMessageToken(String messageToken) {
        this.messageToken = messageToken;
    }

    /**
     * 
     */
    public String getDeviceRegistrationToken() {
        return deviceRegistrationToken;
    }

    /**
     * 
     */
    public void setDeviceRegistrationToken(String deviceRegistrationToken) {
        this.deviceRegistrationToken = deviceRegistrationToken;
    }

    /**
     * 
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 
     */
    public Long getDeviceId() {
        return deviceId;
    }

    /**
     * 
     */
    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return the platformType
     */
    public Short getPlatformType() {
        return platformType;
    }

    /**
     * @param platformType
     *            the platformType to set
     */
    public void setPlatformType(Short platformType) {
        this.platformType = platformType;
    }

    /**
     * @return the initParams
     */
    public Map<String, Object> getInitParams() {
        return initParams;
    }

    /**
     * @param initParams
     *            the initParams to set
     */
    public void setInitParams(Map<String, Object> initParams) {
        this.initParams = initParams;
    }
}
