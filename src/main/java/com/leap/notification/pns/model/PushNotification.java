package com.leap.notification.pns.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * DTO to hold data to be pushed to respective PNS
 * 
 */
public class PushNotification {

    /**
     * Map of payload options. Contains PNS specific options to be included in
     * the final payload being generated for sending to respective PNS
     */
    private Map<String, Object> payloadParams = new HashMap<>();

    /**
     * Contains List of device tokens and maintains respective device delivery
     * status
     */
    private List<PNSMessageDetail> messageDetails = new ArrayList<>();

    /**
     * The message payload to be pushed to the respective PNS
     */
    private String payLoad;

    /**
     * 
     */
    public Map<String, Object> getPayloadParams() {
        return payloadParams;
    }

    /**
     * 
     */
    public void setPayloadParams(Map<String, Object> payloadParams) {
        this.payloadParams = payloadParams;
    }

    /**
     * 
     */
    public String getPayLoad() {
        return payLoad;
    }

    /**
     * 
     */
    public void setPayLoad(String payLoad) {
        this.payLoad = payLoad;
    }

    /**
     * 
     */
    public List<PNSMessageDetail> getMessageDetails() {
        return messageDetails;
    }

    /**
     * 
     */
    public void setMessageDetails(List<PNSMessageDetail> messageDetails) {
        this.messageDetails = messageDetails;
    }

}
