package com.leap.notification.mail.constant;

/**
 * This class is contain host address and port number of the mail servers.
 * 
 */
public interface MailConstant {

    /**
     * IMAP address for gmail mail server.
     */
    String HOST_IMAP_GMAIL = "imap.gmail.com";
    /**
     * IMAP address for yahoo mail server.
     */
    String HOST_IMAP_YAHOO = "imap.mail.yahoo.com";
    /**
     * SMTP address for gmail mail server.
     */
    String HOST_SMTP_GMAIL = "smtp.gmail.com";
    /**
     * SMTP address for yahoo mail server.
     */
    String HOST_SMTP_YAHOO = "smtp.mail.yahoo.com";
    /**
     * SMTP port number for gmail and yahoo mail server.
     */
    String PORT_SMTP_GMAIL_YAHOO = "587";
    /**
     * 
     */
    String MODE_TLS = "useTLS";
    /**
     * 
     */
    String MODE_SSL = "useSSL";

}
