package com.leap.notification.mail.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class models an email message. Message contains content, sender
 * informations and receiver information.
 * 
 */

public class MailMessageDetail {
    /*
     * Constant value for represent the text type content.
     */
    public static final byte CONTENT_TYPE_TEXT = 1;
    /*
     * Constant value for represent the html type content.
     */
    public static final byte CONTENT_TYPE_HTML = 2;
    /**
     * 
     */
    private String subject;
    private String body;
    private byte contentType;
    private Date sendDate;
    private Date receiveDate;
    private String attchmentUrl;
    private String fromAddress;
    private boolean attachment;
    private String bucketName;
    private List<String> toAddresses = new ArrayList<>();
    private List<String> ccAddresses = new ArrayList<>();
    private List<String> bccAddresses = new ArrayList<>();

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public boolean isAttachment() {
        return attachment;
    }

    public void setAttachment(boolean attachment) {
        this.attachment = attachment;
    }


    public String getAttchmentUrl() {
        return attchmentUrl;
    }

    public void setAttchmentUrl(String attchmentUrl) {
        this.attchmentUrl = attchmentUrl;
    }

    /**
     * This method return the message subject.
     * 
     * @return message subject.
     */
    public String getSubject() {
        return subject;
    }

    /**
     * This method set the message subject.
     * 
     * @param subject
     *            message subject.
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * This method return the message content.
     * 
     * @return message content.
     */
    public String getBody() {
        return body;
    }

    /**
     * This method set the message content.
     * 
     * @param body
     *            message content.
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * This method return the message content type.
     * 
     * @return message content type.
     */
    public byte getContentType() {
        return contentType;
    }

    /**
     * This method set the message content type. contentType value are
     * MessageDetail.CONTENT_TYPE_TEXT, MessageDetail.CONTENT_TYPE_HTML etc
     * 
     * @param contentType
     *            message content type.
     */
    public void setContentType(byte contentType) {
        this.contentType = contentType;
    }

    /**
     * This method return the message send date.
     * 
     * @return message send date.
     */
    public Date getSendDate() {
        return sendDate;
    }

    /**
     * This method set the message send date.
     * 
     * @param sendDate
     *            message send date.
     */
    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    /**
     * This method return the message receive date.
     * 
     * @return message receive date.
     */
    public Date getReceiveDate() {
        return receiveDate;
    }

    /**
     * This method set the message receive date.
     * 
     * @param receiveDate
     *            message receive date.
     */
    public void setReceiveDate(Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    /**
     * This method return the sender address.
     * 
     * @return sender address.
     */
    public String getFromAddress() {
        return fromAddress;
    }

    /**
     * This method set the sender address.
     * 
     * @param fromAddress
     *            sender address.
     */
    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    /**
     * This method return list of TO recipient.
     * 
     * @return list of TO recipient.
     */
    public List<String> getToAddresses() {
        return toAddresses;
    }

    /**
     * This method set the recipient for TO.
     * 
     * @param toAddress
     *            recipient for TO.
     */
    public void setToAddress(String toAddress) {
        this.toAddresses.add(toAddress);
    }

    /**
     * This method set the list of recipient for TO.
     * 
     * @param toAddresses
     *            list of recipient for TO.
     */
    public void setToAddresses(List<String> toAddresses) {
        this.toAddresses = toAddresses;
    }

    /**
     * This method return list of BCC recipient.
     * 
     * @return list of CC recipient.
     */
    public List<String> getCcAddresses() {
        return ccAddresses;
    }

    /**
     * This method set the recipient for CC.
     * 
     * @param ccAddress
     *            recipient for CC.
     */
    public void setCcAddress(String ccAddress) {
        this.ccAddresses.add(ccAddress);
    }

    /**
     * This method set the list of recipient for CC.
     * 
     * @param ccAddresses
     *            list of recipient for CC.
     */
    public void setCcAddresses(List<String> ccAddresses) {
        this.ccAddresses = ccAddresses;
    }

    /**
     * This method return list of BCC recipient.
     * 
     * @return list of BCC recipient.
     */
    public List<String> getBccAddresses() {
        return bccAddresses;
    }

    /**
     * This method set the recipient for BCC.
     * 
     * @param bccAddress
     *            recipient for BCC.
     */
    public void setBccAddress(String bccAddress) {
        this.bccAddresses.add(bccAddress);
    }

    /**
     * This method set the list of recipient for BCC.
     * 
     * @param bccAddresses
     *            list of recipient for BCC.
     */
    public void setBccAddresses(List<String> bccAddresses) {
        this.bccAddresses = bccAddresses;
    }

}
