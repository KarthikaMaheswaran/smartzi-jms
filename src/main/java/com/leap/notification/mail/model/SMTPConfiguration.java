/**
 * Copyright (C) MiA - Chalico Ltd.
 * Confidential and proprietary.
 */
package com.leap.notification.mail.model;

import org.springframework.stereotype.Component;

/**
 * This class represent SMTP configuration. SMTPConfiguration contains host
 * address, port, user name and password.
 * 
 */
@Component
public class SMTPConfiguration {
    /**
     * 
     */
    public static final byte GMAIL = 1;
    /**
     * 
     */
    public static final byte YAHOO = 2;
    /**
     * 
     */
    private String host;
    /**
     * 
     */
    private String port;
    /**
     * 
     */
    private String username;
    /**
     * 
     */
    private String password;
    /**
     * 
     */
    private String mode;

    public SMTPConfiguration() {
        // public constructor
    }

    public SMTPConfiguration(String host, String port, String username, String password, String mode) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        this.mode = mode;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

}
