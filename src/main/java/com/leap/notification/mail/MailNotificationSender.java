package com.leap.notification.mail;


import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;

import java.net.URL;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;
import com.leap.notification.mail.constant.MailConstant;
import com.leap.notification.mail.model.MailMessageDetail;
import com.leap.notification.mail.model.SMTPConfiguration;

import javax.activation.DataHandler;
import javax.activation.URLDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * This class is used to send and schedule email using SMTP protocol.
 */

public class MailNotificationSender {
    /**
     *
     */
    private final org.slf4j.Logger LOG = LoggerFactory.getLogger(this.getClass());
    /**
     *
     */
    private MailMessageDetail messageDetail;
    /**
     *
     */
    private SMTPConfiguration smtpConfiguration;
    /**
     *
     */
    public MailNotificationSender() {
        // Default constructor.
    }

    /**
     * Constructor that takes SMTPConfiguration object and MessageDetail object.
     * Used for custom SMTP configuration.
     *
     * @param smtpConfiguration SMTT configuration contain host address, host port number,
     *                          sender user name and password.
     * @param messageDetail     message contain subject, content, receiver addresses, sender
     *                          address.
     */
    public MailNotificationSender(SMTPConfiguration smtpConfiguration, MailMessageDetail messageDetail) {
        this.smtpConfiguration = smtpConfiguration;
        this.messageDetail = messageDetail;
    }

    /**
     * Constructor used for predefined SMTP configuration is available for
     * popular mail service Provider. mailServar value like
     * SMTPConfiguration.GMAIL, SMTPConfiguration.YAHOO etc.
     *
     * @param mailServer    constant value of mail service Provider.
     * @param username      sender user name.
     * @param password      sender password.
     * @param messageDetail message contain subject, content, receiver addresses, sender
     *                      address.
     */
    public MailNotificationSender(byte mailServer, String username, String password, MailMessageDetail messageDetail) {
        this.messageDetail = messageDetail;
        smtpConfiguration = new SMTPConfiguration();
        smtpConfiguration.setUsername(username);
        smtpConfiguration.setPassword(password);
        switch (mailServer) {
            case SMTPConfiguration.GMAIL:
                smtpConfiguration.setHost(MailConstant.HOST_SMTP_GMAIL);
                smtpConfiguration.setPort(MailConstant.PORT_SMTP_GMAIL_YAHOO);
                break;
            case SMTPConfiguration.YAHOO:
                smtpConfiguration.setHost(MailConstant.HOST_SMTP_YAHOO);
                smtpConfiguration.setPort(MailConstant.PORT_SMTP_GMAIL_YAHOO);
                break;
            default:
                smtpConfiguration = null;
                break;
        }
    }

    /**
     * This method return message object.
     *
     * @return message object.
     */
    public MailMessageDetail getMessageDetail() {
        return messageDetail;
    }

    /**
     * This method set message object.
     *
     * @param messageDetail message object.
     */
    public void setMessageDetail(MailMessageDetail messageDetail) {
        this.messageDetail = messageDetail;
    }

    /**
     * This method return SMTP configuration object.
     *
     * @return SMTP Configuration object.
     */
    public SMTPConfiguration getSmtpConfiguration() {
        return smtpConfiguration;
    }

    /**
     * This method set SMTP configuration object.
     *
     * @param smtpConfiguration SMTP configuration object.
     */
    public void setSmtpConfiguration(SMTPConfiguration smtpConfiguration) {
        this.smtpConfiguration = smtpConfiguration;
    }

    /**
     * This method set predefined SMTP configuration is available for popular
     * mail service Provider. mailServar values are SMTPConfiguration.GMAIL,
     * SMTPConfiguration.YAHOO etc
     *
     * @param mailServer constant value of mail service Provider.
     * @param username   sender user name.
     * @param password   sender password.
     */
    public void setSmtpConfiguration(byte mailServer, String username, String password) {
        smtpConfiguration = new SMTPConfiguration();
        smtpConfiguration.setUsername(username);
        smtpConfiguration.setPassword(password);
        switch (mailServer) {
            case SMTPConfiguration.GMAIL:
                smtpConfiguration.setHost(MailConstant.HOST_SMTP_GMAIL);
                smtpConfiguration.setPort(MailConstant.PORT_SMTP_GMAIL_YAHOO);
                break;
            case SMTPConfiguration.YAHOO:
                smtpConfiguration.setHost(MailConstant.HOST_SMTP_YAHOO);
                smtpConfiguration.setPort(MailConstant.PORT_SMTP_GMAIL_YAHOO);
                break;
            default:
                smtpConfiguration = null;
                break;
        }
    }

    /**
     * This method remove null value from receiver list.
     *
     * @param listAddress receiver address list.
     * @return receiver address list.
     */
    private InternetAddress[] getValidAddress(List<String> listAddress) {
        InternetAddress[] internetAddresses = new InternetAddress[0];
        List<InternetAddress> listInternetAddresses = null;
        List<String> nullElement = new ArrayList<>();
        nullElement.add(null);
        if (listAddress != null && listAddress.size() > 0) {
            listInternetAddresses = new ArrayList<>();
            for (String strAddress : listAddress) {
                if (strAddress != null) {
                    try {
                        listInternetAddresses.add(new InternetAddress(strAddress));
                    } catch (AddressException e) {
                        if (LOG.isInfoEnabled()) {
                            LOG.info("An error occured while adding receiver address. Cause " + e.toString());
                        }
                    }
                }
            }
        }
        if (listInternetAddresses != null && listInternetAddresses.size() > 0) {
            internetAddresses = new InternetAddress[listInternetAddresses.size()];
            int count = 0;
            for (InternetAddress internetAddress : listInternetAddresses) {
                internetAddresses[count] = internetAddress;
                count++;
            }
        }
        return internetAddresses;
    }

    /**
     * This method send the message to the specified addresses.
     *
     * @return this method will return true if success otherwise false.
     */
    public boolean sendMessage() throws MailException {
        boolean isSuccess = false;
        boolean isSendRequired = false;
        Session smtpSession = null;
        try {
            Properties smtpProperties = System.getProperties();
            smtpProperties.put("mail.smtp.auth", "true");
            smtpProperties.put("mail.smtp.sendpartial", "true");
            smtpProperties.put("mail.smtp.host", smtpConfiguration.getHost());
            smtpProperties.put("mail.smtp.port", smtpConfiguration.getPort());
            if (MailConstant.MODE_SSL.equals(smtpConfiguration.getMode())) {
                smtpProperties.put("mail.smtp.ssl.enable", "true");
            } else if (MailConstant.MODE_TLS.equals(smtpConfiguration.getMode())) {
                smtpProperties.put("mail.smtp.starttls.enable", "true");
            }
            smtpSession = Session.getInstance(smtpProperties, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(smtpConfiguration.getUsername(), smtpConfiguration.getPassword());
                }
            });
            MimeMessage mineMessage = new MimeMessage(smtpSession);
            mineMessage.setHeader("Content-Transfer-Encoding", "quoted-printable");
            if (messageDetail.getBody() != null) {
                switch (messageDetail.getContentType()) {
                    case MailMessageDetail.CONTENT_TYPE_TEXT:
                        mineMessage.setHeader("Content-Type", "text/plain; charset=\"utf-8\"");
                        mineMessage.setContent(messageDetail.getBody(), "text/plain; charset=utf-8");
                        break;
                    case MailMessageDetail.CONTENT_TYPE_HTML:
                        mineMessage.setHeader("Content-Type", "text/html; charset=\"utf-8\"");
                        mineMessage.setContent(messageDetail.getBody(), "text/html; charset=utf-8");
                        break;
                    default:
                        mineMessage.setHeader("Content-Type", "text/plain; charset=\"utf-8\"");
                        mineMessage.setContent(messageDetail.getBody(), "text/plain; charset=utf-8");
                        break;
                }
            } else {
                mineMessage.setContent(" ", "text/plain; charset=utf-8");
            }
            if (messageDetail.getSubject() != null) {
                mineMessage.setSubject(messageDetail.getSubject(), "utf-8");
            } else {
                mineMessage.setSubject(" ", "utf-8");
            }
            mineMessage.setFrom(new InternetAddress(smtpConfiguration.getUsername()));
            InternetAddress[] toAddresses = getValidAddress(messageDetail.getToAddresses());
            if (toAddresses != null && toAddresses.length > 0) {
                mineMessage.setRecipients(Message.RecipientType.TO, toAddresses);
                isSendRequired = true;
            }
            InternetAddress[] ccAddresses = getValidAddress(messageDetail.getCcAddresses());
            if (ccAddresses != null && ccAddresses.length > 0) {
                mineMessage.setRecipients(Message.RecipientType.CC, ccAddresses);
                isSendRequired = true;
            }
            InternetAddress[] bccAddresses = getValidAddress(messageDetail.getBccAddresses());
            if (bccAddresses != null && bccAddresses.length > 0) {
                mineMessage.setRecipients(Message.RecipientType.BCC, bccAddresses);
                isSendRequired = true;
            }
            if (messageDetail.isAttachment()) {
                System.out.println("attachment URL*********"+messageDetail.getAttchmentUrl());
                System.out.println("attachment*********"+messageDetail.isAttachment());
                System.out.println("s3 bucket name*********"+messageDetail.getBucketName());

                // create the Multipart and add its parts to it
                Multipart mpart = new MimeMultipart();
                // create the second message part
                MimeBodyPart messageBodyPart = new MimeBodyPart();
                URL url = new URL(GeneratePresignedUrlAndUploadObject(messageDetail.getAttchmentUrl(),messageDetail.getBucketName()));
                System.out.println("S3 sort url*********"+url);

                URLDataSource uds = new URLDataSource(url);
                messageBodyPart.setDataHandler(new DataHandler(uds));
                messageBodyPart.setDisposition(Part.ATTACHMENT);
                messageBodyPart.setFileName(url.getFile());
                System.out.println("fdatasource*********"+url.getFile());

                // add the Multipart to the message
                mpart.addBodyPart(messageBodyPart);
                mineMessage.setContent(mpart);
            }
            if (isSendRequired) {
                Transport.send(mineMessage);
            }

            isSuccess = true;
        }
        catch (Exception exception)
        {
            System.out.println("exception email*"+exception);
        }
      /*  catch (MessagingException e) {
            throw new MailException(e.getMessage(), e);
        }*/
        return isSuccess;
    }
public String GeneratePresignedUrlAndUploadObject(String objectKey,String bucketName) throws IOException {
    URL url = null;
    try {
        AWSCredentialsProvider credentialProvider = InstanceProfileCredentialsProvider
                .createAsyncRefreshingProvider(false);
        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(credentialProvider)
                .enablePathStyleAccess()
                .build();


   /*     AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withRegion(clientRegion)
                .withCredentials(new ProfileCredentialsProvider())
                .build();
*/
        // Set the presigned URL to expire after one hour.
        java.util.Date expiration = new java.util.Date();
        long expTimeMillis = expiration.getTime();
/*
        expTimeMillis += 1000 * 60 * 60;
*/
        expTimeMillis += 30000;

        expiration.setTime(expTimeMillis);

        // Generate the presigned URL.
        System.out.println("Generating pre-signed URL.");
        //s3Client.getObject(bucketName,objectKey);
        GeneratePresignedUrlRequest generatePresignedUrlRequest =
                new GeneratePresignedUrlRequest(bucketName, objectKey)
                        .withMethod(HttpMethod.GET)
                        .withExpiration(expiration);
         url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);


        System.out.println("Pre-Signed file: "+s3Client.generatePresignedUrl(generatePresignedUrlRequest).getFile());
        System.out.println("Pre-Signed path: "+s3Client.generatePresignedUrl(generatePresignedUrlRequest).getPath());


    } catch (AmazonServiceException e) {
        // The call was transmitted successfully, but Amazon S3 couldn't process
        // it, so it returned an error response.
        e.printStackTrace();
    } catch (SdkClientException e) {
        // Amazon S3 couldn't be contacted for a response, or the client
        // couldn't parse the response from Amazon S3.
        e.printStackTrace();
    }
   // return url.toString().replace("https","http");
    return url.toString();
}
}
