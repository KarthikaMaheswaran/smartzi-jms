package com.leap.common.constant;


/**
 * Enum of different types of application
 */
public enum AppTypeEnum {
    driverapp((short) 1, "driverapp"), customerapp((short) 2, "customerapp");

    /**
     *
     */
    private final Short id;
    /**
     *
     */
    private final String appType;
    /**
     *
     */
    private AppTypeEnum(Short id, String appType) {
        this.id = id;
        this.appType = appType;
    }

    /**
     *
     */
    public Short getId() {
        return id;
    }

    /**
     *
     */
    public String getAppType() {
        return appType;
    }

}
