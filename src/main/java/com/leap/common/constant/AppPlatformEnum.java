package com.leap.common.constant;

/**
 * Enum of different types of application platform
 */
public enum AppPlatformEnum {
    android((short) 1, "android"), ios((short) 2, "ios"),web((short)3,"web");

    /**
     *
     */
    private final Short id;
    /**
     *
     */
    private final String appPlatform;
    /**
     *
     */
    private AppPlatformEnum(Short id, String appPlatform) {
        this.id = id;
        this.appPlatform = appPlatform;
    }

    /**
     *
     */
    public Short getId() {
        return id;
    }

    /**
     *
     */
    public String getAppPlatform() {
        return appPlatform;
    }

}
