package com.leap.common.constant;
/**
 * Copyright (C) MiA - Chalico Ltd.
 * Confidential and proprietary.
 */

/**
 * Log messages to go here
 */
public class LogMessage {

    /**
     *
     */
    public static final String ERROR_OCCURRED_WHILE_MAPPING_UPDATE_CUSTOMER_PROFILE_REQ_FROM_WEB_PORTAL = "An error occurred while "
            + "mapping the update customer profile request from web portal . Cause : ";
    /**
     *
     */
    public static final String ERROR_OCCURRED_WHILE_UPDATING_CUSTOMER_PROFILE_FROM_WEB_PORTAL = "An error occurred while updating "
            + "customer profile from web portal . Cause : ";
    /**
     *
     */
    public static final String ERROR_OCCURRED_WHILE_MAPPING_UPDATE_DRIVER_PROFILE_REQ = "An error occurred while mapping the update "
            + "driver profile request . Cause : ";
    /**
     *
     */
    public static final String ERROR_OCCURRED_WHILE_UPDATING_DRIVER_PROFILE = "An error occurred while updating driver profile . Cause : ";
    /**
     *
     */
    public static final String ERROR_OCCURRED_WHILE_CLOSING_CALLABLE_STATEMENT_OR_RESULT_SET = "An error occurred while "
            + "closing the CallableStatement or Resultset, Cause :";
    /**
     *
     */
    public static final String SECONDS = " seconds";
    /**
     *
     */
    public static final String DB_EXCEPTION_OCCURRED = "DB Exception occurred, Cause : ";
    /**
     *
     */
    public static final String ERROR_OCCURRED_WHILE_CREATING_NOTIF_REQ_JSON = "An error occurred while creating notification request JSON, Cause : ";
    /**
     *
     */
    public static final String ERROR_OCCURRED_WHILE_PUSHING_NOTIF_REQ_TO_QUEUE = "An error occurred while pushing notification request to queue, Cause : ";
    /**
     *
     */
    public static final String ERROR = "Error : ";
    /**
     *
     */
    public static final String CAUSED_BY = " and caused by : ";
    /**
     *
     */
    public static final String TROUBLE_WITH_SERVER = "Sorry, something went wrong. Please try again later.";
    /**
     *
     */
    public static final String ACCOUNT_DEACTIVATED = "Your account has been deactivated. Please contact Smartzi customer suppport.";
    /**
     *
     */
    public static final String UTF = "UTF-8";
    /**
     *
     */
    public static final String ERROR_WHILE_HASHING = "Error while hashing";
    /**
     *
     */
    public static final String EXCLUDING_FROM_SELECTION = ", Hence excluding from selection";
    /**
     *
     */
    public static final String USER_AUTHENTICATION_FAILED = "User authentication failed";

    public static final String DOCUMENT_TAG_FAILED = "Unable to tag a document, update tag failed";
}
