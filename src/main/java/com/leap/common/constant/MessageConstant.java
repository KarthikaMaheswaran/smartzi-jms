package com.leap.common.constant;

// TODO: Auto-generated Javadoc

/**
 * The Class MessageConstant.
 */
public class MessageConstant {

    /**
     * Instantiates a new message constant.
     */
    private MessageConstant() {
        // Private constructor
    }

    /**
     * The Constant SUCCESS.
     */
    public static final String SUCCESS = "success";

    /**
     * The Constant FILE_UPLOAD_SUCCESS.
     */
    public static final String FILE_UPLOAD_SUCCESS = "File uploaded successfully";

    /**
     * The Constant PASSWORD_CHANGE_SUCCESS.
     */
    public static final String PASSWORD_CHANGE_SUCCESS = "Password is changed successfully.";

    /**
     * The Constant REGISTER.
     */
    public static final String REGISTER = "Registration Successful !";

    /**
     * The Constant SETPRICE.
     */

    public static final String SETPRICE = "Set Trip Rate Successful !";

    /**
     * The Constant GET_DRIVER_STATIC_DATA_SUCCESS.
     */
    public static final String GET_DRIVER_STATIC_DATA_SUCCESS = "Fetching driver static data Successfull !";

    /**
     * The Constant GET_DRIVER_PHYSICAL_VERIFICATION_DOCUMENTS_SUCCESS.
     */
    public static final String GET_DRIVER_PHYSICAL_VERIFICATION_DOCUMENTS_SUCCESS = "Success !";

    /**
     * The Constant SUCCESS_FORGOT_PASSWORD_LINK_SEND.
     */
    public static final String SUCCESS_FORGOT_PASSWORD_LINK_SEND = "Verification link has been sent to the registered mail address.";

    /**
     * The Constant SUCCESS_PASSWORD_CHANGED.
     */
    public static final String SUCCESS_PASSWORD_CHANGED = "Your password is changed successfully.";

    /**
     * The Constant SUCCESS_ADD_PREFERDRIVER.
     */
    public static final String SUCCESS_ADD_PREFERDRIVER = "prefer driver Added sucessfully.";

    /**
     * The Constant DRIVER_CARD_STATUS.
     */
    public static final String DRIVER_CARD_STATUS = "Driver has not registered card";

    /**
     * The Constant UPDATED_APP_VERSION.
     */
    public static final String UPDATED_APP_VERSION = "New version of the app is available to download. Please update and take advantage of the new features.";

    /**
     * The Constant CREADTED_CARD_REGISTER.
     */
    public static final String CREADTED_CARD_REGISTER = "Card added successfuly.";


}
