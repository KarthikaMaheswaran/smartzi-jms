package com.leap;

import com.leap.exception.JmsErrorHandler;
import com.leap.jms.JMSListenerTest;
import com.leap.notification.mail.model.SMTPConfiguration;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.RedeliveryPolicy;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;
import org.springframework.web.client.RestTemplate;

import javax.jms.ConnectionFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


@ComponentScan(basePackages = "com.leap.*")
@SpringBootApplication
@EnableJms
public class SmartziJMSListener extends SpringBootServletInitializer {

	@Value("${leap.smtp.host}")
	private String host;

	@Value("${leap.smtp.port}")
	private String port;

	@Value("${leap.smtp.username}")
	private String username;

	@Value("${leap.smtp.password}")
	private String password;

	@Value("${leap.smtp.mode}")
	private String mode;

	@Value("${leap.email.from}")
	private String emailFromLeap;

	@Value("${leap.clickatel.bearer.token}")
	private String clickaTelBearerToken;

	@Value("${leap.driver.p12.path}")
	private String driverP12Path;

	@Value("${leap.driver.p12.pwd}")
	private String driverP12Pass;

	@Value("${leap.driver.p12.usetest}")
	private String driverP12UseTest;

	@Value("${leap.driver.fcm.sender.id}")
	private String driverFCMSenderId;

	@Value("${leap.customer.p12.path}")
	private String customerP12Path;

	@Value("${leap.customer.p12.pwd}")
	private String customerP12Pass;

	@Value("${leap.customer.p12.usetest}")
	private String customerP12UseTest;

	@Value("${leap.customer.fcm.sender.id}")
	private String customerFCMSenderId;



	@Autowired
	JMSListenerTest jmsListenerTest;

	@Autowired
	JmsErrorHandler jmsErrorHandler;

	@Value("${spring.activemq.broker-url}")
	private String springUrl;

	@Value("${queue.priority.low}")
	private String listenerContainer1;

	@Value("${queue.priority.high}")
	private String listenerContainer3;

	@Value("${queue.priority.medium}")
	private String listenerContainer2;

	public static void main(String[] args) {
		System.out.println("started");
		SpringApplication.run(SmartziJMSListener.class, args);
	}
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(SmartziJMSListener.class);
	}
	/**
	 * Rest template.
	 *
	 * @return the rest template
	 */
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}


	/**
	 * Task scheduler.
	 *
	 * @return the task scheduler
	 */
	@Bean
	public TaskScheduler taskScheduler() {
		return new ConcurrentTaskScheduler(); // single threaded by default
	}

	/**
	 * Property sources placeholder configurer.
	 *
	 * @return the property sources placeholder configurer
	 */
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean("velocityEngine")
	public VelocityEngine velocityEngine() throws VelocityException, IOException {
		VelocityEngineFactoryBean factoryBean = new VelocityEngineFactoryBean();
		factoryBean.setResourceLoaderPath("classpath:vmfiles");
		factoryBean.setPreferFileSystemAccess(false);
		VelocityEngine velocityEngine = factoryBean.createVelocityEngine();
		velocityEngine.init();
		return velocityEngine;
	}

	@Bean("smtpConfiguration")
	SMTPConfiguration smtpConfiguration() {
		SMTPConfiguration smtpConfiguration = new SMTPConfiguration();
		smtpConfiguration.setHost(host);
		smtpConfiguration.setMode(mode);
		smtpConfiguration.setUsername(username);
		smtpConfiguration.setPassword(password);
		smtpConfiguration.setPort(port);
		return smtpConfiguration;
	}

	@Bean("appConfig")
	Properties appConfig(){
		Properties properties = new Properties();
		properties.setProperty("emailFromLeap",emailFromLeap);
		properties.setProperty("clickaTelBearerToken",clickaTelBearerToken);
		properties.setProperty("driverP12Path",driverP12Path);
		properties.setProperty("driverP12Pass",driverP12Pass);
		properties.setProperty("driverP12UseTest",driverP12UseTest);
		properties.setProperty("driverFCMSenderId",driverFCMSenderId);
		properties.setProperty("customerP12Path",customerP12Path);
		properties.setProperty("customerP12Pass",customerP12Pass);
		properties.setProperty("customerP12UseTest",customerP12UseTest);
		properties.setProperty("customerFCMSenderId",customerFCMSenderId);
		return properties;
	}


	@Bean
	public JmsTemplate jmsTemplate(){
		JmsTemplate jmsTemplate= new JmsTemplate();
		jmsTemplate.setConnectionFactory(connectionFactory());
		return jmsTemplate;
	}
	@Bean
	public ConnectionFactory connectionFactory() {
		List<String> filesList = new ArrayList<>();
		filesList.add("producer.NotificationMessage");
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
		connectionFactory.setTrustAllPackages(true);
		connectionFactory.setTrustedPackages(filesList);
		connectionFactory.setRedeliveryPolicy(redeliveryPolicy());
		return connectionFactory;
	}

	@Bean
	public ConnectionFactory dlqConnectionFactory() {
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
		connectionFactory.setBrokerURL(springUrl);
		connectionFactory.setRedeliveryPolicy(redeliveryPolicy());
		return connectionFactory;
	}

	@Bean("low_priority.queue")
	public DefaultMessageListenerContainer listenerContainer1() {
		DefaultMessageListenerContainer defaultMessageListenerContainer = new DefaultMessageListenerContainer();
		defaultMessageListenerContainer.setConnectionFactory(connectionFactory());
		defaultMessageListenerContainer.setDestinationName(listenerContainer1);
		defaultMessageListenerContainer.setMessageListener(jmsListenerTest);
		defaultMessageListenerContainer.setMaxConcurrentConsumers(5);
		defaultMessageListenerContainer.setSessionTransacted(true);
		defaultMessageListenerContainer.setErrorHandler(jmsErrorHandler);
		return defaultMessageListenerContainer;
	}

	@Bean("medium_priority.queue")
	public DefaultMessageListenerContainer listenerContainer2() {
		DefaultMessageListenerContainer defaultMessageListenerContainer = new DefaultMessageListenerContainer();
		defaultMessageListenerContainer.setConnectionFactory(connectionFactory());
		defaultMessageListenerContainer.setDestinationName(listenerContainer2);
		defaultMessageListenerContainer.setMessageListener(jmsListenerTest);
		defaultMessageListenerContainer.setMaxConcurrentConsumers(20);
		defaultMessageListenerContainer.setSessionTransacted(true);
		defaultMessageListenerContainer.setErrorHandler(jmsErrorHandler);
		return defaultMessageListenerContainer;
	}

	@Bean("high_priority.queue")
	public DefaultMessageListenerContainer listenerContainer3() {
		DefaultMessageListenerContainer defaultMessageListenerContainer = new DefaultMessageListenerContainer();
		defaultMessageListenerContainer.setConnectionFactory(connectionFactory());
		defaultMessageListenerContainer.setDestinationName(listenerContainer3);
		defaultMessageListenerContainer.setMessageListener(jmsListenerTest);
		defaultMessageListenerContainer.setMaxConcurrentConsumers(50);
		defaultMessageListenerContainer.setSessionTransacted(true);
		defaultMessageListenerContainer.setErrorHandler(jmsErrorHandler);
		return defaultMessageListenerContainer;
	}

	@Bean("dlqListenerContainer")
	public DefaultMessageListenerContainer dlqListenerContainer() {
		DefaultMessageListenerContainer defaultMessageListenerContainer = new DefaultMessageListenerContainer();
		defaultMessageListenerContainer.setConnectionFactory(dlqConnectionFactory());
		defaultMessageListenerContainer.setDestination(dlqDestination());
		defaultMessageListenerContainer.setMessageListener(jmsListenerTest);
		defaultMessageListenerContainer.setMaxConcurrentConsumers(50);
		defaultMessageListenerContainer.setSessionTransacted(true);
		return defaultMessageListenerContainer;
	}


	@Bean
	public RedeliveryPolicy redeliveryPolicy() {
		RedeliveryPolicy redeliveryPolicy = new RedeliveryPolicy();
		redeliveryPolicy.setBackOffMultiplier(1);
		redeliveryPolicy.setInitialRedeliveryDelay(15000);
		redeliveryPolicy.setMaximumRedeliveries(0);
		redeliveryPolicy.setRedeliveryDelay(15000);
		redeliveryPolicy.setUseExponentialBackOff(true);
		return redeliveryPolicy;

	}

	@Bean
	public ActiveMQQueue dlqDestination() {
		return new ActiveMQQueue("ActiveMQ.DLQ");
	}
}
