package com.leap;

import com.leap.jms.PropertyConfig;
import com.leap.notification.mail.model.SMTPConfiguration;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Configuration
public class NotificationConfig {



//    @Bean("velocityEngine")
//    public VelocityEngineFactoryBean velocityEngine() {
//        VelocityEngineFactoryBean velocityEngineFactoryBean = new VelocityEngineFactoryBean();
//        velocityEngineFactoryBean.setResourceLoaderPath("/vmfiles/");
//        velocityEngineFactoryBean.setPreferFileSystemAccess(false);
//        return velocityEngineFactoryBean;
//    }




    @Bean("sendOTP")
    HashMap sendOTP() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/sendOTP.vm");
        map.put("email", "email/sendOTP.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("SmartziCPCustomerAccountRegistered")
    HashMap SmartziCPCustomerAccountRegistered() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/SmartziCPCustomerAccountRegistered.vm");
        map.put("email", "email/SmartziCPCustomerAccountRegistered.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("reSendOTP")
    HashMap reSendOTP() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/ReSendOTP.vm");
        map.put("email", "email/ReSendOTP.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("sendOTPForForgotPassword")
    HashMap sendOTPForForgotPassword() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/SendOTPForForgotPassword.vm");
        map.put("email", "email/SendOTPForForgotPassword.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("driverReg")
    HashMap driverReg() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/DriverReg.vm");
        map.put("email", "email/DriverReg.vm");
        map.put("pns", "pns/DriverReg.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("forgotPassword")
    HashMap forgotPassword() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/ForgotPassword.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("paymentSuccessful")
    HashMap paymentSuccessful() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/PaymentSuccessful.vm");
        map.put("email", "email/PaymentSuccessful.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("customerSuccessValidateOTP")
    HashMap customerSuccessValidateOTP() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/CustomerSuccessValidateOTP.vm");
        map.put("email", "email/CustomerSuccessValidateOTP.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("driverValidateOTPSuccess")
    HashMap driverValidateOTPSuccess() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/DriverValidateOTPSuccess.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("tripInitiateStatus")
    HashMap tripInitiateStatus() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/TripInitiateStatus.vm");
        map.put("pns", "pns/TripInitiateStatus.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("rideReady")
    HashMap rideReady() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/RideReady.vm");
        map.put("pns", "pns/RideReady.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("startTrip")
    HashMap startTrip() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/StartTrip.vm");
        map.put("pns", "pns/StartTrip.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("endTrip")
    HashMap endTrip() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/EndTrip.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("bookingCom")
    HashMap bookingCom() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/BookingComEmail.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("flightDelayDriverNotification")
    HashMap flightDelayDriverNotification() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/FlightDelayDriverNotification.vm");
        map.put("sms", "sms/FlightDelayDriverNotification.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("customerTripCancellation")
    HashMap customerTripCancellation() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/CustomerTripCancellation.vm");
        map.put("sms", "sms/CustomerTripCancellation.vm");
        map.put("email", "email/CustomerTripCancellation.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("driverTripCancellation")
    HashMap driverTripCancellation() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/DriverTripCancellation.vm");
        map.put("sms", "sms/DriverTripCancellation.vm");
        map.put("email", "email/DriverTripCancellation.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("driverDispatchV3")
    HashMap driverDispatchV3() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/DriverDispatchV3.vm");
        map.put("sms", "sms/DriverDispatchV3.vm");
        map.put("email", "email/DriverDispatchV3.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("customerPasswordReset")
    HashMap customerPasswordReset() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/customerPasswordReset.vm");
        map.put("sms", "sms/customerPasswordReset.vm");
        map.put("email", "email/customerPasswordReset.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("customerTripCancellationForDriver")
    HashMap customerTripCancellationForDriver() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/CustomerTripCancellationForDriver.vm");
        map.put("sms", "sms/CustomerTripCancellationForDriver.vm");
        map.put("email", "email/CustomerTripCancellationForDriver.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("driverTripCancellationFromDriverApp")
    HashMap driverTripCancellationFromDriverApp() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/DriverTripCancellationFromDriverApp.vm");
        map.put("sms", "sms/DriverTripCancellationFromDriverApp.vm");
        map.put("email", "email/DriverTripCancellationFromDriverApp.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("unassignDriverFromBooking")
    HashMap unassignDriverFromBooking() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/UnassignDriverFromBooking.vm");
        map.put("sms", "sms/UnassignDriverFromBooking.vm");
        map.put("email", "email/UnassignDriverFromBooking.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("bookingConfirmationDriverFoundOndemand")
    HashMap bookingConfirmationDriverFoundOndemand() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/BookingConfirmationDriverFoundOndemand.vm");
        map.put("sms", "sms/BookingConfirmationDriverFoundOndemand.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("bookingConfirmDriverFoundOnDemandForDriverApp")
    HashMap bookingConfirmDriverFoundOnDemandForDriverApp() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/BookingConfirmDriverFoundOnDemandForDriverApp.vm");
        map.put("sms", "sms/BookingConfirmDriverFoundOnDemandForDriverApp.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("BookingConfirmDriverFoundOnDemandForDriverAppForAirport")
    HashMap BookingConfirmDriverFoundOnDemandForDriverAppForAirport() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/BookingConfirmDriverFoundOnDemandForDriverAppForAirport.vm");
        map.put("sms", "sms/BookingConfirmDriverFoundOnDemandForDriverAppForAirport.vm");
        map.put("email", "email/BookingConfirmDriverFoundOnDemandForDriverAppForAirport.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("bookingConfirmTripAssignForDriverAppForAirport")
    HashMap bookingConfirmTripAssignForDriverAppForAirport() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/bookingConfirmTripAssignForDriverAppForAirport.vm");
        map.put("sms", "sms/bookingConfirmTripAssignForDriverAppForAirport.vm");
        map.put("email", "email/bookingConfirmTripAssignForDriverAppForAirport.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("bookingConfirmTripEditForDriverAppForAirport")
    HashMap bookingConfirmTripEditForDriverAppForAirport() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/BookingConfirmTripEditForDriverAppForAirport.vm");
        map.put("sms", "sms/BookingConfirmTripEditForDriverAppForAirport.vm");
        map.put("email", "email/BookingConfirmTripEditForDriverAppForAirport.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("BookingConfirmDriverFoundPreBookingForDriverAppForAirport")
    HashMap BookingConfirmDriverFoundPreBookingForDriverAppForAirport() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/BookingConfirmDriverFoundPreBookingForDriverAppForAirport.vm");
        map.put("sms", "sms/BookingConfirmDriverFoundPreBookingForDriverAppForAirport.vm");
        map.put("email", "email/BookingConfirmDriverFoundPreBookingForDriverAppForAirport.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("bookingConfirmDriverFoundPreBookingForDriverApp")
    HashMap bookingConfirmDriverFoundPreBookingForDriverApp() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/BookingConfirmDriverFoundPreBookingForDriverApp.vm");
        map.put("sms", "sms/BookingConfirmDriverFoundPreBookingForDriverApp.vm");
        map.put("email", "email/BookingConfirmDriverFoundPreBookingForDriverApp.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("bookingConfirmTripEditedForDriverApp")
    HashMap bookingConfirmTripEditedForDriverApp() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/BookingConfirmTripEditedForDriverApp.vm");
        map.put("sms", "sms/BookingConfirmTripEditedForDriverApp.vm");
        map.put("email", "email/BookingConfirmTripEditedForDriverApp.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("bookingConfirmationDriverFoundPrebooking")
    HashMap bookingConfirmationDriverFoundPrebooking() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/BookingConfirmationDriverFoundPrebooking.vm");
        map.put("sms", "sms/BookingConfirmationDriverFoundPrebooking.vm");
        map.put("email", "email/BookingConfirmationDriverFoundPrebooking.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("bookingConfirmationUpdatedDetails")
    HashMap bookingConfirmationUpdatedDetails() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/BookingConfirmationUpdatedDetails.vm");
        map.put("sms", "sms/BookingConfirmationUpdatedDetails.vm");
        map.put("email", "email/BookingConfirmationUpdatedDetails.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("bookingConfirmationUpdatedDetailsWithMultiStop")
    HashMap bookingConfirmationUpdatedDetailsWithMultiStop() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/BookingConfirmationUpdatedDetails.vm");
        map.put("sms", "sms/BookingConfirmationUpdatedDetails.vm");
        map.put("email", "email/BookingConfirmationUpdatedDetails.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("jobReassignToAdmin")
    HashMap jobReassignToAdmin() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/JobReassignToAdmin.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("adminNotes")
    HashMap adminNotes() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/AdminNotes.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("bookingConfirmationDriverNotFoundPrebooking")
    HashMap bookingConfirmationDriverNotFoundPrebooking() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/BookingConfirmationDriverNotFoundPrebooking.vm");
        map.put("sms", "sms/BookingConfirmationDriverNotFoundPrebooking.vm");
        map.put("email", "email/BookingConfirmationDriverNotFoundPrebooking.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("cardPaymentSuccessCustomer")
    HashMap cardPaymentSuccessCustomer() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/CardPaymentSuccessCustomer.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("accountPaymentSuccessCustomer")
    HashMap accountPaymentSuccessCustomer() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/AccountPaymentSuccessCustomer.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("cardCouponPaymentSuccessCustomer")
    HashMap cardCouponPaymentSuccessCustomer() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/CardCouponPaymentSuccessCustomer.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("cardPaymentSuccessDriver")
    HashMap cardPaymentSuccessDriver() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/CardPaymentSuccessDriver.vm");
        map.put("email", "email/CardPaymentSuccessDriver.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("cardPaymentFailedCustomer")
    HashMap cardPaymentFailedCustomer() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/CardPaymentFailedCustomer.vm");
        map.put("email", "email/CardPaymentFailedCustomer.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("cardPaymentFailedDriver")
    HashMap cardPaymentFailedDriver() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/CardPaymentFailedDriver.vm");
        map.put("sms", "sms/CardPaymentFailedDriver.vm");
        map.put("email", "email/CardPaymentFailedDriver.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("cashPaymentCustomer")
    HashMap cashPaymentCustomer() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/CashPaymentCustomer.vm");
        hashMap.putAll(map);
        return hashMap;
    }


    @Bean("cashCouponPaymentCustomer")
    HashMap cashCouponPaymentCustomer() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/cashCouponPaymentCustomer.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("cashPaymentDriver")
    HashMap cashPaymentDriver() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/CashPaymentDriver.vm");
        map.put("email", "email/CashPaymentDriver.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("driverFoundForBooking")
    HashMap driverFoundForBooking() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/DriverFoundForBooking.vm");
        map.put("sms", "sms/DriverFoundForBooking.vm");
        map.put("email", "email/DriverFoundForBooking.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("driverFoundInRetryCustomerApp")
    HashMap driverFoundInRetryCustomerApp() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/DriverFoundInRetryCustomerApp.vm");
        map.put("sms", "sms/DriverFoundInRetryCustomerApp.vm");
        map.put("email", "email/DriverFoundInRetryCustomerApp.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("driverFoundInRetryDriverApp")
    HashMap driverFoundInRetryDriverApp() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/DriverFoundInRetryDriverApp.vm");
        map.put("sms", "sms/DriverFoundInRetryDriverApp.vm");
        map.put("email", "email/DriverFoundInRetryDriverApp.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("driverDebitFirstNotify")
    HashMap driverDebitFirstNotify() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/DriverDebitFirstNotify.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("driverDebitSecondAttemptSuccessWeekly")
    HashMap driverDebitSecondAttemptSuccessWeekly() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/DriverDebitSecondAttemptSuccessWeekly.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("driverDebitSecondAttemptSuccessMonthly")
    HashMap driverDebitSecondAttemptSuccessMonthly() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/DriverDebitSecondAttemptSuccessMonthly.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean
    HashMap driverDebitFirstNotifyForMonthly() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/DriverDebitFirstNotifyForMonthly.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("driverDebitFirstAttemptFailWeekly")
    HashMap driverDebitFirstAttemptFailWeekly() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/DriverDebitFirstAttemptFailWeekly.vm");
        map.put("sms", "sms/DriverDebitFirstAttemptFailWeekly.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("driverDebitFirstAttemptSuccessWeekly")
    HashMap driverDebitFirstAttemptSuccessWeekly() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/DriverDebitFirstAttemptSuccessWeekly.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("driverDebitFirstAttemptFailMonthly")
    HashMap driverDebitFirstAttemptFailMonthly() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/DriverDebitFirstAttemptFailMonthly.vm");
        map.put("sms", "sms/DriverDebitFirstAttemptFailMonthly.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("driverDebitFirstAttemptSuccessMonthly")
    HashMap driverDebitFirstAttemptSuccessMonthly() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/DriverDebitFirstAttemptSuccessMonthly.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("driverNotFoundInRetry")
    HashMap driverNotFoundInRetry() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/DriverNotFoundInRetry.vm");
        map.put("sms", "sms/DriverNotFoundInRetry.vm");
        map.put("email", "email/DriverNotFoundInRetry.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("driverDebitSecondAttemptFailWeekly")
    HashMap driverDebitSecondAttemptFailWeekly() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/DriverDebitSecondAttemptFailWeekly.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("driverDebitSecondAttemptFailMonthly")
    HashMap driverDebitSecondAttemptFailMonthly() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/DriverDebitSecondAttemptFailMonthly.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("broadcastMessage")
    HashMap broadcastMessage() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/BroadcastMessage.vm");
        map.put("sms", "sms/BroadcastMessage.vm");
        map.put("email", "email/BroadcastMessage.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("adminBookingConfirmationOnDemandForCustomer")
    HashMap adminBookingConfirmationOnDemandForCustomer() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/AdminBookingConfirmationOnDemandForCustomer.vm");
        map.put("sms", "sms/AdminBookingConfirmationOnDemandForCustomer.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("adminBookingConfirmationOnDemandForDriver")
    HashMap adminBookingConfirmationOnDemandForDriver() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/AdminBookingConfirmationOnDemandForDriver.vm");
        map.put("sms", "sms/AdminBookingConfirmationOnDemandForDriver.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("adminBookingConfirmationPreBookingForCustomer")
    HashMap adminBookingConfirmationPreBookingForCustomer() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/AdminBookingConfirmationPreBookingForCustomer.vm");
        map.put("sms", "sms/AdminBookingConfirmationPreBookingForCustomer.vm");
        map.put("email", "email/AdminBookingConfirmationPreBookingForCustomer.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("adminReAssignToNextDriver")
    HashMap adminReAssignToNextDriver() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/AdminReAssignToNextDriver.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("adminBookingConfirmationPreBookingForDriver")
    HashMap adminBookingConfirmationPreBookingForDriver() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/AdminBookingConfirmationPrebookingForDriver.vm");
        map.put("sms", "sms/AdminBookingConfirmationPrebookingForDriver.vm");
        map.put("email", "email/AdminBookingConfirmationPrebookingForDriver.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("adminBlacklistedCutstomerAccount")
    HashMap adminBlacklistedCutstomerAccount() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/AdminBlacklistedCutstomerAccount.vm");
        map.put("sms", "sms/AdminBlacklistedCutstomerAccount.vm");
        map.put("email", "email/AdminBlacklistedCutstomerAccount.vm");
        hashMap.putAll(map);
        return hashMap;
    }

    @Bean("adminDeActivatedCutstomerAccount")
    HashMap adminDeActivatedCutstomerAccount() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/AdminDeActivatedCutstomerAccount.vm");
        map.put("sms", "sms/AdminDeActivatedCutstomerAccount.vm");
        map.put("email", "email/AdminDeActivatedCutstomerAccount.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("adminDriverActivation")
    HashMap adminDriverActivation() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/AdminActivatedDriverAccount.vm");
        map.put("sms", "sms/AdminActivatedDriverAccount.vm");
        map.put("email", "email/AdminActivatedDriverAccount.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("adminRejectedDriver")
    HashMap adminRejectedDriver() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/AdminRejectedDriver.vm");
        map.put("sms", "sms/AdminRejectedDriver.vm");
        map.put("email", "email/AdminRejectedDriver.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("adminDeActivatedDriver")
    HashMap adminDeActivatedDriver() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/AdminDeActivatedDriver.vm");
        map.put("sms", "sms/AdminDeActivatedDriver.vm");
        map.put("email", "email/AdminDeActivatedDriver.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("adminBlackListedDriver")
    HashMap adminBlackListedDriver() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/AdminBlacklistedDriver.vm");
        map.put("sms", "sms/AdminBlacklistedDriver.vm");
        map.put("email", "email/AdminBlacklistedDriver.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("adminSendDriverInfo")
    HashMap adminSendDriverInfo() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/AdminSendDriverMoreInfo.vm");
        map.put("sms", "sms/AdminSendDriverMoreInfo.vm");
        map.put("email", "email/AdminSendDriverMoreInfo.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("adminResetPassword")
    HashMap adminResetPassword() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/AdminResetsPassword.vm");
        map.put("sms", "sms/AdminResetsPassword.vm");
        map.put("email", "email/AdminResetsPassword.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("FutureTripDriverFound")
    HashMap FutureTripDriverFound() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/Futuretripalertforadminusers.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("FutureTripDriverNotFound")
    HashMap FutureTripDriverNotFound() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/FutureTripDriverNotFound.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("Prebookingalerttoadmin")
    HashMap Prebookingalerttoadmin() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/Prebookingalerttoadmin.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("documentExpiryAlertWeekBefore")
    HashMap documentExpiryAlertWeekBefore() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/AdminDriverDocumentNearExpiry1Week.vm");
        map.put("email", "email/AdminDriverDocumentNearExpiry1Week.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("documentExpiryAlert2DaysBefore")
    HashMap documentExpiryAlert2DaysBefore() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/AdminDriverDocumentNearExpiry2Days.vm");
        map.put("email", "email/AdminDriverDocumentNearExpiry2Days.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("prebooking_acknowledge_customer")
    HashMap prebooking_acknowledge_customer() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/prebooking_acknowledge_customer.vm");
        map.put("sms", "sms/prebooking_acknowledge_customer.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("blockdriver")
    HashMap blockdriver() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/blockdriver.vm");
        map.put("email", "email/blockdriver.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("NewBookingRequestForDriver")
    HashMap NewBookingRequestForDriver() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "pns/NewBookingRequestForDriver.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("adminBookingConfirmationAccountBookingForCustomer")
    HashMap adminBookingConfirmationAccountBookingForCustomer() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/AdminBookingConfirmationAccountBookingForCustomer.vm");
        map.put("sms", "sms/AdminBookingConfirmationAccountBookingForCustomer.vm");
        map.put("email", "email/AdminBookingConfirmationAccountBookingForCustomer.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("driverSelfBookingTriptoAdmin")
    HashMap driverSelfBookingTriptoAdmin() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/DriverSelfBookingTriptoAdmin.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("bookingConfirmDriverAdminNotes")
    HashMap bookingConfirmDriverAdminNotes() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/ControllerAdminNotes.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("invoiceRegenerated")
    HashMap invoiceRegenerated() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/DriverInvoiceRegenerated.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("updateInvoice")
    HashMap updateInvoice() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/updateInvoice.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("airportTripConfirmation")
    HashMap airportTripConfirmation() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/airportTripConfirmation.vm");
        map.put("email", "email/airportTripConfirmation.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("sendB2BUserIdPassword")
    HashMap sendB2BUserIdPassword() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/SendB2BUserIdPassword.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("B2BForgotPassword")
    HashMap B2BForgotPassword() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/B2BForgotPassword.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("B2BCustomerUserNamePsw")
    HashMap B2BCustomerUserNamePsw() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/B2BCustomerUserNamePsw.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("SendOTPCommon")
    HashMap SendOTPCommon() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/SendOTPCommon.vm");
        map.put("email", "email/SendOTPCommon.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("SendJobsToDriverPool")
    HashMap SendJobsToDriverPool() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/SendJobsToDriverPool.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("GiveRideBackReassignToAdmin")
    HashMap GiveRideBackReassignToAdmin() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/GiveRideBackReassignToAdmin.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("SendAirportRegistrationPassword")
    HashMap SendAirportRegistrationPassword() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("email", "email/SendAirportRegistrationPassword.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("bookingConfirmCustomerForBooking")
    HashMap bookingConfirmCustomerForBooking() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/BookingConfirmCustomerForBooking.vm");
        map.put("sms", "sms/BookingConfirmCustomerForBooking.vm");
        map.put("email", "email/BookingConfirmCustomerForBooking.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("bookingConfirmCustomerForBookingAcceptDriver")
    HashMap bookingConfirmCustomerForBookingAcceptDriver() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/BookingConfirmCustomerForBookingAcceptDriver.vm");
        map.put("sms", "sms/BookingConfirmCustomerForBookingAcceptDriver.vm");
        map.put("email", "email/BookingConfirmCustomerForBookingAcceptDriver.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("bookingConfirmCustomerForBookingMultiStop")
    HashMap bookingConfirmCustomerForBookingMultiStop() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/BookingConfirmCustomerForBookingMultiStop.vm");
        map.put("sms", "sms/BookingConfirmCustomerForBookingMultiStop.vm");
        map.put("email", "email/BookingConfirmCustomerForBookingMultiStop.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("bookingConfirmDriverFoundForBooking")
    HashMap bookingConfirmDriverFoundForBooking() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/BookingConfirmDriverFoundForBooking.vm");
        map.put("sms", "sms/BookingConfirmDriverFoundForBooking.vm");
        map.put("email", "email/BookingConfirmDriverFoundForBooking.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("driverRequestDriverFoundPreBookingForDriverApp")
    HashMap driverRequestDriverFoundPreBookingForDriverApp() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/DriverRequestDriverFoundPreBookingForDriverApp.vm");
        map.put("sms", "sms/DriverRequestDriverFoundPreBookingForDriverApp.vm");
        map.put("email", "email/DriverRequestDriverFoundPreBookingForDriverApp.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("driverRequestForDriverAppForAirport")
    HashMap driverRequestForDriverAppForAirport() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("pns", "pns/DriverRequestForDriverAppForAirport.vm");
        map.put("sms", "sms/DriverRequestForDriverAppForAirport.vm");
        map.put("email", "email/DriverRequestForDriverAppForAirport.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("driverDocumentNearExpiryDays")
    HashMap driverDocumentNearExpiryDays() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/DriverDocumentNearExpiryDays.vm");
        map.put("email", "email/DriverDocumentNearExpiryDays.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("driverDocumentNearExpiryOneWeek")
    HashMap driverDocumentNearExpiryOneWeek() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/DriverDocumentNearExpiryOneWeek.vm");
        map.put("email", "email/DriverDocumentNearExpiryOneWeek.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("driverDocumentNearExpiryTwoWeek")
    HashMap driverDocumentNearExpiryTwoWeek() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/DriverDocumentNearExpiryTwoWeek.vm");
        map.put("email", "email/DriverDocumentNearExpiryTwoWeek.vm");
        hashMap.putAll(map);
        return hashMap;
    }
    @Bean("documentExpairyDriverDeactivate")
    HashMap documentExpairyDriverDeactivate() {
        HashMap hashMap = new HashMap();
        Map map = new HashMap();
        map.put("sms", "sms/DocumentExpairyDriverDeactivate.vm");
        map.put("email", "email/DocumentExpairyDriverDeactivate.vm");
        hashMap.putAll(map);
        return hashMap;
    }




}

