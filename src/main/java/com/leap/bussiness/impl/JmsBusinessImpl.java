package com.leap.bussiness.impl;

import com.leap.bussiness.JmsBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import com.leap.jms.dto.NotificationMessage;
import com.leap.jms.dto.request.NotificationRequest;
import com.leap.jms.JMSHelperTest;

import javax.jms.JMSException;

@RestController
public class JmsBusinessImpl implements JmsBusiness {

    @Autowired
    JMSHelperTest jmsHelperTest;

    @Override
    public void sendNotification(NotificationRequest notificationRequest) throws JMSException {
        if (notificationRequest != null) {
            NotificationMessage notificationMessage1 = new NotificationMessage(notificationRequest.getNotificationGUID());
            jmsHelperTest.pushToQueue(notificationMessage1, notificationRequest.getPriority());
        }
    }
}
