package com.leap.bussiness;

import com.leap.jms.dto.request.NotificationRequest;

import javax.jms.JMSException;

public interface JmsBusiness {

    public void sendNotification(NotificationRequest notificationRequest) throws JMSException;
}
